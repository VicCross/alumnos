/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import java.util.ArrayList;
import org.hibernate.Transaction;
import org.hibernate.Session;
import model.entities.Alumno;

/**
 *
 * @author Home
 */
public class AlumnoDaoImplement implements AlumnoDao {
    Transaction transaccion;
    Session sesion;
    
    @Override
    public boolean agregar(Alumno alumnoAct) {
        try
        {
            sesion = NewHibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            sesion.save(alumnoAct);
            transaccion.commit();
            sesion.close();
            return true;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return false;
        }
    }
    
    @Override
    public ArrayList<Alumno> listar() {
        try
        {
            sesion = NewHibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            ArrayList<Alumno> alumnos = (ArrayList<Alumno>) sesion.createQuery("from Alumno").list();
            sesion.close();
            return alumnos;
        }
        catch(Exception e)
        {
            return null;
        }
    }
    
}
