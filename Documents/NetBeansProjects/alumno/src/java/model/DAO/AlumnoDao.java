/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import java.util.ArrayList;
import model.entities.Alumno;

/**
 *
 * @author Home
 */
public interface AlumnoDao {
    
    public boolean agregar (Alumno alumnoAct);
    public ArrayList<Alumno> listar ();
    
}
