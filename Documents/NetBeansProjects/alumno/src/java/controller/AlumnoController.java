/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.ArrayList;
import model.DAO.AlumnoDao;
import model.DAO.AlumnoDaoImplement;
import model.entities.Alumno;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

/**
 *
 * @author Home
 */
public class AlumnoController extends ActionSupport implements ModelDriven<Alumno>  {

    private Alumno alumnoAct = new Alumno();
    private ArrayList<Alumno> listaAlumnos = new ArrayList();
    private String msg;
    AlumnoDao alumnoDaoAct;
    @Override
    public Alumno getModel() {
        return getAlumnoAct();
    }
    
    public AlumnoController ()
    {
        alumnoDaoAct = new AlumnoDaoImplement();
    }
    
    @Action(value="/agregarAlumnoInicio", results={@Result(name="success", location="/vista/agregarAlumno.jsp")})
    public String agregarInicio()
    {
        return SUCCESS;
    }
    
    @Action(value="/listarAlumnosInicio", results={@Result(name="success", location="/vista/listarAlumnos.jsp")})
    public String listarInicio()
    {
        return SUCCESS;
    }
    
    @Action(value="/agregarAlumno", results={@Result(name="success", location="/vista/agregarAlumno.jsp")})
    public String agregar()
    {
        if(alumnoDaoAct.agregar(alumnoAct))
        {
            setMsg("Se agrego un nuevo registro");
        }
        else
        {
            setMsg("No se pudo agregar el alumno");
        }
        return SUCCESS;
    }
    
    @Action(value="/listarAlumnos", results={@Result(name="success", location="/vista/listarAlumnos.jsp")})
    public String listar()
    {
        setListaAlumnos(alumnoDaoAct.listar());
        return "success";
    }

    /**
     * @return the alumnoAct
     */
    public Alumno getAlumnoAct() {
        return alumnoAct;
    }

    /**
     * @param alumnoAct the alumnoAct to set
     */
    public void setAlumnoAct(Alumno alumnoAct) {
        this.alumnoAct = alumnoAct;
    }

    /**
     * @return the listaAlumnos
     */
    public ArrayList<Alumno> getListaAlumnos() {
        return listaAlumnos;
    }

    /**
     * @param listaAlumnos the listaAlumnos to set
     */
    public void setListaAlumnos(ArrayList<Alumno> listaAlumnos) {
        this.listaAlumnos = listaAlumnos;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }
    
}
