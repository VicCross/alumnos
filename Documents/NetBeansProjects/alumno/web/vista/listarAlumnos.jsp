<%-- 
    Document   : listarCliente
    Created on : 19/08/2016, 10:52:16 AM
    Author     : Home
--%>

<%@page import="model.entities.Alumno"%>
<%@page import="java.util.ArrayList"%>
<%@page import="controller.AlumnoController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alumnos</title>
    </head>
    <body>
        <h1>Lista de Alumnos</h1>
        <br>
        <%
            try
            {
                AlumnoController alumnoAct = new AlumnoController();
                alumnoAct.listar();
                ArrayList <Alumno> alumnos = alumnoAct.getListaAlumnos();
                out.println("<table>");
                for(Alumno alumnoBucle : alumnos)
                {
                    out.println("<tr>");
                    out.println("<td>id = " + alumnoBucle.getIdAlumno()+"</td>");
                    out.println("<td>Nombre = " + alumnoBucle.getNombre()+"</td>");
                    out.println("</tr>");
                }
                out.println("<table>");
            }
            catch(Exception e)
            {
                out.println(e);
            }
        %>
        
        
    </body>
</html>
