<%-- 
    Document   : AgregarCliente
    Created on : 19/08/2016, 10:52:01 AM
    Author     : Home
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agregar Alumno</title>
    </head>
    <body>
        <h1>Agregar Alumno</h1>
        <br>
        <s:form action="agregarAlumno" method="post">
            <s:textfield label="Nombre del Alumno" name="alumnoAct.nombre" size="20" maxLength="30" />
            <s:submit value="Almacenar alumno" />
        </s:form>
        <br>
        <s:label value="%{msg}" />
    </body>
</html>
