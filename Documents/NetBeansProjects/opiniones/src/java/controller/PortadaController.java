/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import model.DAO.AreaDAOImp;
import model.DAO.OpinionDAOImp;

/**
 *
 * @author VicCross
 */
public class PortadaController extends ActionSupport
{
    private ArrayList <Clasificacion> clasificaciones;
    
    private int idArea;
    private int idClasificacion;
    private String area;
    
    public String obtenerContadores()
    {
        OpinionDAOImp opinionDAOAct = new OpinionDAOImp();
        AreaDAOImp areaDAOAct = new AreaDAOImp();
        setClasificaciones(opinionDAOAct.consultarClasificacion(getIdArea()));
        area = areaDAOAct.consultarArea(idArea);
        return SUCCESS;
    }

    /**
     * @param idArea the idArea to set
     */
    public void setIdArea(int idArea) {
        this.idArea = idArea;
    }

    /**
     * @return the clasificaciones
     */
    public ArrayList <Clasificacion> getClasificaciones() {
        return clasificaciones;
    }

    /**
     * @param clasificaciones the clasificaciones to set
     */
    public void setClasificaciones(ArrayList <Clasificacion> clasificaciones) {
        this.clasificaciones = clasificaciones;
    }

    /**
     * @return the idArea
     */
    public int getIdArea() {
        return idArea;
    }

    /**
     * @return the area
     */
    public String getArea() {
        return area;
    }
}
