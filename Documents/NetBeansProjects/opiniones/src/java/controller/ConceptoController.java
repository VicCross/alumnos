/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.List;
import model.DAO.ConceptoDAOImp;
import model.entities.CConcepto;

/**
 *
 * @author VicCross
 */
public class ConceptoController extends ActionSupport{
    
    private List<List<CConcepto>> conceptos = new ArrayList<>();
    private List<List<Concepto>> conceptosExt = new ArrayList<>();
    private int idArea;
    private int idProyecto;
    private ConceptoDAOImp conceptoDAOAct = new ConceptoDAOImp();
    
    public String listarConceptoArea ()
    {
        ArrayList<CConcepto> concepts = new ArrayList<>();
        concepts = getConceptoDAOAct().listarArea(idArea);
        if(!concepts.isEmpty())
        {
            for(CConcepto actividad: concepts)
            {
                if(actividad.getCConcepto() == null)
                { 
                    List<CConcepto> nueva = new ArrayList<CConcepto>();
                    nueva.add(actividad);
                    conceptos.add(nueva);
                }
                else
                {
                    for(List<CConcepto> concepto: conceptos)
                    {
                        for(int n = concepto.size()-1; n >= 0; n-- )
                        {
                            if(concepto.get(n).getIdConcepto() == actividad.getCConcepto().getIdConcepto())
                            {
                                concepto.add(actividad);
                            }
                        }
                    }
                }
            }
        }
        System.out.println("I'm so restless");
        copiarConceptos();
        return SUCCESS;
    }
    
    public String listarConceptoProyecto ()
    {
        
        ArrayList<CConcepto> concepts = new ArrayList<>();
        concepts = getConceptoDAOAct().listarProyecto(getIdProyecto());
        if(!concepts.isEmpty())
        {
            for(CConcepto actividad: concepts)
            {
                if(actividad.getCConcepto().getCConcepto() == null)
                { 
                    List<CConcepto> nueva = new ArrayList<CConcepto>();
                    nueva.add(actividad);
                    conceptos.add(nueva);
                }
                else
                {
                    for(List<CConcepto> concepto: conceptos)
                    {
                        for(int n = concepto.size()-1; n >= 0; n-- )
                        {
                            if(concepto.get(n).getCConcepto().getIdConcepto() == actividad.getCConcepto().getCConcepto().getIdConcepto())
                            {
                                concepto.add(actividad);
                            }
                        }
                    }
                }
            }
        }
        System.out.println("I'm so restless");
        copiarConceptos();
        return SUCCESS;
    }
    
    private void copiarConceptos()
    {
        for (List<CConcepto> concepto : conceptos)
        {
            List<Concepto> conceptosPlanos = new ArrayList<>();
            for(CConcepto conceptoAct: concepto)
            {
                Concepto conceptoPlanoAct = new Concepto();
                conceptoPlanoAct.setIdConcepto(conceptoAct.getIdConcepto());
                conceptoPlanoAct.setNombre(conceptoAct.getNombre());
                conceptoPlanoAct.setNivel(conceptoAct.getCNivelConcepto().getIdNivelConcepto());
                conceptosPlanos.add(conceptoPlanoAct);
            }
            getConceptosExt().add(conceptosPlanos);
        }
    }

    /**
     * @return the idArea
     */
    public int getIdArea() {
        return idArea;
    }

    /**
     * @param idArea the idArea to set
     */
    public void setIdArea(int idArea) {
        this.idArea = idArea;
    }

    /**
     * @return the conceptoDAOAct
     */
    public ConceptoDAOImp getConceptoDAOAct() {
        return conceptoDAOAct;
    }

    /**
     * @param conceptoDAOAct the conceptoDAOAct to set
     */
    public void setConceptoDAOAct(ConceptoDAOImp conceptoDAOAct) {
        this.conceptoDAOAct = conceptoDAOAct;
    }

    /**
     * @return the conceptosExt
     */
    public List<List<Concepto>> getConceptosExt() {
        return conceptosExt;
    }

    /**
     * @param conceptosExt the conceptosExt to set
     */
    public void setConceptosExt(List<List<Concepto>> conceptosExt) {
        this.conceptosExt = conceptosExt;
    }

    /**
     * @return the idProyecto
     */
    public int getIdProyecto() {
        return idProyecto;
    }

    /**
     * @param idProyecto the idProyecto to set
     */
    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }
    
}
