/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import model.DAO.TipoOpinionDAOImp;
import model.DAO.interfaces.TipoOpinionDAO;
import model.entities.CTipoOpinion;

/**
 *
 * @author VicCross
 */
public class TipoController extends ActionSupport{
    
    private CTipoOpinion tipoOpinionAct = new CTipoOpinion();
    private ArrayList<CTipoOpinion> listaTipos = new ArrayList();
    TipoOpinionDAO tipoOpinionDAOAct;
    
    public TipoController ()
    {
        tipoOpinionDAOAct = new TipoOpinionDAOImp();
    }
    
    public String listar()
    {
        setListaTipos(tipoOpinionDAOAct.listar());
        return SUCCESS;
    }

    /**
     * @return the tipoOpinionAct
     */
    public CTipoOpinion getTipoOpinionAct() {
        return tipoOpinionAct;
    }

    /**
     * @param tipoOpinionAct the tipoOpinionAct to set
     */
    public void setTipoOpinionAct(CTipoOpinion tipoOpinionAct) {
        this.tipoOpinionAct = tipoOpinionAct;
    }

    /**
     * @return the listaTipos
     */
    public ArrayList<CTipoOpinion> getListaTipos() {
        return listaTipos;
    }

    /**
     * @param listaTipos the listaTipos to set
     */
    public void setListaTipos(ArrayList<CTipoOpinion> listaTipos) {
        this.listaTipos = listaTipos;
    }
}
