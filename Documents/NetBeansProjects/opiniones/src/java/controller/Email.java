/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author VicCross
 */
public class Email {
    private String usuario = "";
    private String password = "";
    
    public boolean EnviarCorreo(String destinatario, String asunto, String mensaje)
    {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        
        boolean exito = false;
        
        Session session = Session.getInstance(props,
		  new javax.mail.Authenticator()
                  {
			protected PasswordAuthentication getPasswordAuthentication()
                        {
				return new PasswordAuthentication(getUsuario(), getPassword());
			}
		  });
        try
        {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(getUsuario()));
            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(destinatario));
            message.setSubject(asunto);
            message.setText(mensaje);
            Transport.send(message);
            exito = true;
        }
        catch (Exception e)
        {
            exito = false;
        }
        return exito;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
