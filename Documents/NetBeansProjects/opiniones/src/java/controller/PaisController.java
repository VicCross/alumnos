/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import model.DAO.DelegacionDAOImp;
import model.DAO.EstadoDAOImp;
import model.DAO.PaisDAOImp;
import model.entities.CDelegacion;
import model.entities.CEstado;
import model.entities.CPais;
/*import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;*/

/**
 *
 * @author VicCross
 */

public class PaisController extends ActionSupport{
    
    private CPais paisAct = new CPais();
    private int idPais;
    private int estado;
    private ArrayList<CPais> paises = new ArrayList();
    private ArrayList<CEstado> estados = new ArrayList();
    private ArrayList<CDelegacion> delegaciones = new ArrayList();
    PaisDAOImp paisDAOImpAct = new PaisDAOImp();
    EstadoDAOImp estadoDAOImpAct = new EstadoDAOImp();
    DelegacionDAOImp delegacionDAOImp = new DelegacionDAOImp();
    
    public String listar()
    {
        setPaises(paisDAOImpAct.listar());
        return SUCCESS;
    }
    
    //@Action(value="/listarEstados", results={@Result(name="success", type="json")})
    public String listarEstados()
    {
        setEstados(estadoDAOImpAct.listar(this.idPais));
        return SUCCESS;
        
    }
    
    public String listarDelegaciones()
    {
        setDelegaciones(delegacionDAOImp.listar(this.estado));
        return SUCCESS;
        
    }

    /**
     * @return the paisAct
     */
    public CPais getPaisAct() {
        return paisAct;
    }

    /**
     * @param paisAct the paisAct to set
     */
    public void setPaisAct(CPais paisAct) {
        this.paisAct = paisAct;
    }

    /**
     * @return the paises
     */
    public ArrayList<CPais> getPaises() {
        return paises;
    }

    /**
     * @param paises the paises to set
     */
    public void setPaises(ArrayList<CPais> paises) {
        this.paises = paises;
    }

    /**
     * @return the idPais
     */
    public int getIdPais() {
        return idPais;
    }

    /**
     * @param idPais the idPais to set
     */
    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    /**
     * @return the estados
     */
    public ArrayList<CEstado> getEstados() {
        return estados;
    }

    /**
     * @param estados the estados to set
     */
    public void setEstados(ArrayList<CEstado> estados) {
        this.estados = estados;
    }

    /**
     * @return the delegaciones
     */
    public ArrayList<CDelegacion> getDelegaciones() {
        return delegaciones;
    }

    /**
     * @param delegaciones the delegaciones to set
     */
    public void setDelegaciones(ArrayList<CDelegacion> delegaciones) {
        this.delegaciones = delegaciones;
    }

    /**
     * @return the estado
     */
    public int getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }
    
}
