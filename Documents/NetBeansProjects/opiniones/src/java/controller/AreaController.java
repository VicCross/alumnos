/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import model.DAO.AreaDAOImp;
import model.entities.CArea;

/**
 *
 * @author VicCross
 */
public class AreaController extends ActionSupport {
     private ArrayList<CArea> areas = new ArrayList<>();
     private AreaDAOImp areaDAOAct;
     
    public String listar()
    {
        areas = areaDAOAct.listar();
        return SUCCESS;
    }
    
}
