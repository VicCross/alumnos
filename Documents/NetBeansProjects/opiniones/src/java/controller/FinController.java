/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import model.DAO.OpinionDAOImp;

/**
 *
 * @author VicCross
 */
public class FinController extends ActionSupport{
    private int[] chkOpiniones;
    
    public String finalizarOpinion ()
    {
        OpinionDAOImp opinionDAOAct = new OpinionDAOImp();
        boolean exito = false;
        exito = opinionDAOAct.finalizar(chkOpiniones);
        return SUCCESS;
    }

    /**
     * @param chkOpiniones the chkOpiniones to set
     */
    public void setChkOpiniones(int[] chkOpiniones) {
        this.chkOpiniones = chkOpiniones;
    }
    
}
