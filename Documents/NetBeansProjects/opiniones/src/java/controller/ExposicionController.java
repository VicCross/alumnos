/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import model.DAO.interfaces.ExposicionDAO;
import model.DAO.ExposicionDAOImp;
import model.entities.CExposicion;

/**
 *
 * @author VicCross
 */
public class ExposicionController extends ActionSupport {
    
    private CExposicion exposicionAct = new CExposicion();
    private ArrayList<CExposicion> exposiciones = new ArrayList();
    ExposicionDAO exposicionDAOAct;
    
    public ExposicionController ()
    {
        exposicionDAOAct = new ExposicionDAOImp();
    }
    
    public String mostrar()
    {
        exposiciones = exposicionDAOAct.mostrar();
        return SUCCESS;
    }

    /**
     * @return the exposicionAct
     */
    public CExposicion getExposicionAct() {
        return exposicionAct;
    }

    /**
     * @param exposicionAct the exposicionAct to set
     */
    public void setExposicionAct(CExposicion exposicionAct) {
        this.exposicionAct = exposicionAct;
    }

    /**
     * @return the exposiciones
     */
    public ArrayList<CExposicion> getExposiciones() {
        return exposiciones;
    }

    /**
     * @param exposiciones the exposiciones to set
     */
    public void setExposiciones(ArrayList<CExposicion> exposiciones) {
        this.exposiciones = exposiciones;
    }
}
