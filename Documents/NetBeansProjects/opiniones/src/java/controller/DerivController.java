/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import model.DAO.AreaDAOImp;
import model.DAO.OpinionDAOImp;
import model.DAO.ProyectoDAOImp;
import model.entities.CArea;
import model.entities.CConcepto;
import model.entities.CProyecto;
import model.entities.CTipoOpinion;
import model.entities.KCopia;
import model.entities.KCopiaId;
import model.entities.KOpinion;

/**
 *
 * @author VicCross
 */
public class DerivController extends ActionSupport
{
    private int idOpinion;
    private KOpinion opinionAct;
    private OpinionDAOImp opinionDAO = new OpinionDAOImp();
    private ArrayList<CArea> areas = new ArrayList<>();
    private AreaDAOImp areaDAOAct = new AreaDAOImp();
    private ArrayList<CProyecto> proyectos = new ArrayList<>();
    private ProyectoDAOImp proyectoDAOAct = new ProyectoDAOImp();
    
    private int idTipo;
    private int cbActividades;
    private int cbMetas;
    private int cbArea;
    private int[] copiasArea;
    private int[] copiasProyecto;
    
    public String consultarOpinion()
    {
        try
        {
            opinionAct = new KOpinion();
            opinionAct = opinionDAO.consultar(idOpinion);
            setAreas(getAreaDAOAct().listar());
            setProyectos(getProyectoDAOAct().listar());
            return SUCCESS;
        }
        catch(Exception e)
        {
            return ERROR;
        }
    }
    
    public String derivar()
    {
        try
        {
            Date now = new Date();
            int idConcepto = 0;
            if(cbActividades != 0)
            {
                idConcepto = cbActividades;
            }
            else if (cbMetas != 0)
            {
                idConcepto = cbMetas;
            }
            opinionAct = new KOpinion();
            opinionAct = opinionDAO.consultar(idOpinion);
            int ynnaf = opinionAct.getCTipoOpinion().getIdTipo();
            if(opinionAct.getClasificacion() != idTipo)
            {
                opinionAct.setClasificacion(idTipo);
            }
            CConcepto conceptoAct = new CConcepto();
            conceptoAct.setIdConcepto(idConcepto);
            opinionAct.setCConcepto(conceptoAct);
            opinionAct.setStatusOpinion(3);
            opinionAct.setFechaUltimaModificacion(now);
            opinionAct.setUsuarioUltimaModificacion("VicCross");
            opinionAct.setFechaTurno(now);
            
            Set copias = new HashSet();
            if (copiasArea != null)
            {
                for(int i=0; i < copiasArea.length; i++)
                {
                    KCopia copiaAct = new KCopia();
                    KCopiaId idCopiaAct = new KCopiaId();
                    idCopiaAct.setIdArea(copiasArea[i]);
                    idCopiaAct.setIdOpinion(opinionAct.getIdOpinion());
                    copiaAct.setId(idCopiaAct);
                    copiaAct.setTipo(1);
                    copiaAct.setKOpinion(opinionAct);
                    copiaAct.setUsuarioCreacion("VicCross");
                    copiaAct.setFechaCreacion(now);
                    copiaAct.setUsuarioUltimaModificacion("VicCross");
                    copiaAct.setFechaUltimaModificacion(now);
                    copiaAct.setPantalla("derivOpinion.jsp");
                    copias.add(copiaAct);
                }
            }
            
            if (copiasProyecto != null)
            {
                for(int i=0; i < copiasProyecto.length; i++)
                {
                    KCopia copiaAct = new KCopia();
                    KCopiaId idCopiaAct = new KCopiaId();
                    idCopiaAct.setIdArea(copiasProyecto[i]);
                    idCopiaAct.setIdOpinion(opinionAct.getIdOpinion());
                    copiaAct.setId(idCopiaAct);
                    copiaAct.setTipo(2);
                    copiaAct.setKOpinion(opinionAct);
                    copiaAct.setUsuarioCreacion("VicCross");
                    copiaAct.setFechaCreacion(now);
                    copiaAct.setUsuarioUltimaModificacion("VicCross");
                    copiaAct.setFechaUltimaModificacion(now);
                    copiaAct.setPantalla("derivOpinion.jsp");
                    copias.add(copiaAct);
                }
            }
            
            opinionAct.setKCopias(copias);
            boolean exito = false;
            exito = opinionDAO.turnar(opinionAct);
            if (exito == true)
            {
                Email correoAct = new Email();
                correoAct.setUsuario("vic150cc@gmail.com");
                correoAct.setPassword("monsoon.58");
                String clasificacion = "";
                switch (opinionAct.getClasificacion()) {
                    case 1:
                        clasificacion = "felicitación";
                        break;
                    case 2:
                        clasificacion = "petición";
                        break;
                    case 3:
                        clasificacion = "sugerencia";
                        break;
                    case 4:
                        clasificacion = "queja";
                        break;
                    default:
                        break;
                }
                boolean correoExito = false;
                if(cbActividades != 0)
                {
                    CArea areaAct = areaDAOAct.consultar(cbArea);
                    correoExito = correoAct.EnviarCorreo(areaAct.getCorreo(), "Nueva Opinion Derivada", "<h1> Tu área: " + areaAct.getNombre() + " \n Ha recibido una "+clasificacion);
                }
                else
                {
                    correoExito = true;
                }
                if (correoExito == true)
                {
                    return SUCCESS;
                }
                else
                {
                    return ERROR;
                }
            }
            else
            {
                return ERROR;
            }
        }
        catch(Exception e)
        {
            return ERROR;
        }
    }
        
        
        
    // <editor-fold defaultstate="collapsed" desc="getters y setters">
    /**
     * @return the idOpinion
     */
    public int getIdOpinion() {
        return idOpinion;
    }

    /**
     * @param idOpinion the idOpinion to set
     */
    public void setIdOpinion(int idOpinion) {
        this.idOpinion = idOpinion;
    }

    /**
     * @return the opinionAct
     */
    public KOpinion getOpinionAct() {
        return opinionAct;
    }

    /**
     * @param opinionAct the opinionAct to set
     */
    public void setOpinionAct(KOpinion opinionAct) {
        this.opinionAct = opinionAct;
    }

    /**
     * @return the opinionDAO
     */
    public OpinionDAOImp getOpinionDAO() {
        return opinionDAO;
    }

    /**
     * @param opinionDAO the opinionDAO to set
     */
    public void setOpinionDAO(OpinionDAOImp opinionDAO) {
        this.opinionDAO = opinionDAO;
    }
    
    // </editor-fold>

    /**
     * @return the areas
     */
    public ArrayList<CArea> getAreas() {
        return areas;
    }

    /**
     * @param areas the areas to set
     */
    public void setAreas(ArrayList<CArea> areas) {
        this.areas = areas;
    }

    /**
     * @return the areaDAOAct
     */
    public AreaDAOImp getAreaDAOAct() {
        return areaDAOAct;
    }

    /**
     * @param areaDAOAct the areaDAOAct to set
     */
    public void setAreaDAOAct(AreaDAOImp areaDAOAct) {
        this.areaDAOAct = areaDAOAct;
    }

    /**
     * @return the proyectos
     */
    public ArrayList<CProyecto> getProyectos() {
        return proyectos;
    }

    /**
     * @param proyectos the proyectos to set
     */
    public void setProyectos(ArrayList<CProyecto> proyectos) {
        this.proyectos = proyectos;
    }

    /**
     * @return the proyectoDAOAct
     */
    public ProyectoDAOImp getProyectoDAOAct() {
        return proyectoDAOAct;
    }

    /**
     * @param proyectoDAOAct the proyectoDAOAct to set
     */
    public void setProyectoDAOAct(ProyectoDAOImp proyectoDAOAct) {
        this.proyectoDAOAct = proyectoDAOAct;
    }

    /**
     * @param tipoOpinion the tipoOpinion to set
     */
    public void setIdTipo(int tipoOpinion) {
        this.idTipo = tipoOpinion;
    }

    /**
     * @param copiasArea the copiasArea to set
     */
    public void setCopiasArea(int[] copiasArea) {
        this.copiasArea = copiasArea;
    }

    /**
     * @param copiasProyecto the copiasProyecto to set
     */
    public void setCopiasProyecto(int[] copiasProyecto) {
        this.copiasProyecto = copiasProyecto;
    }
    
    /**
     * @param cbActividades the cbActividades to set
     */
    public void setCbActividades(int cbActividades) {
        this.cbActividades = cbActividades;
    }

    /**
     * @param cbMetas the cbMetas to set
     */
    public void setCbMetas(int cbMetas) {
        this.cbMetas = cbMetas;
    }

    /**
     * @param cbArea the cbArea to set
     */
    public void setCbArea(int cbArea) {
        this.cbArea = cbArea;
    }
}
