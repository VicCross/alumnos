/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author VicCross
 */
public class Clasificacion {
    private int leidas;
    private int noLeidas;
    private int totales;
    
    public void calculoTotales ()
    {
        this.setTotales(this.getLeidas() + this.getNoLeidas());
    }

    /**
     * @return the leidas
     */
    public int getLeidas() {
        return leidas;
    }

    /**
     * @param leidas the leidas to set
     */
    public void setLeidas(int leidas) {
        this.leidas = leidas;
    }

    /**
     * @return the noLeidas
     */
    public int getNoLeidas() {
        return noLeidas;
    }

    /**
     * @param noLeidas the noLeidas to set
     */
    public void setNoLeidas(int noLeidas) {
        this.noLeidas = noLeidas;
    }

    /**
     * @return the totales
     */
    public int getTotales() {
        return totales;
    }

    /**
     * @param totales the totales to set
     */
    public void setTotales(int totales) {
        this.totales = totales;
    }
}
