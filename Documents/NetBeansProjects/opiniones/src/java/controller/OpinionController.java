/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import model.DAO.interfaces.OpinionDAO;
import model.DAO.OpinionDAOImp;
import model.entities.CConcepto;
import model.entities.CExposicion;
import model.entities.CRedSocial;
import model.entities.CTipoOpinion;
import model.entities.KOpinion;
import model.entities.KPersonaOpinion;
import model.entities.KServidorDenunciado;

/**
 *
 * @author VicCross
 */
public class OpinionController extends ActionSupport {
    
    private KOpinion opinionAct = new KOpinion();
    private KPersonaOpinion personaAct;
    private ArrayList<KOpinion> opiniones = new ArrayList();
    private ArrayList<Opinion> opinionesExt = new ArrayList<>();
    private OpinionDAO opinionDAOAct;
    private int cbEcposicion;
    private int cbTipoOpinion;
    private int idTipo;
    private String txtFolio;
    private Date txtFecha;
    private String txtNombre;
    private int txtEdad;
    private String txtApellidoPat;
    private String txtEmail;
    private String txtApellidoMat;
    private String txtTelefono;
    private String txtUrl;
    private String txtPais;
    private String txtEntidadFederativa;
    private String txtDelegacion;
    private String txtColonia;
    private String txtCalle;
    private String txtNumExt;
    private String txtCp;
    private String txtNumInt;
    private String txtNombreServidor;
    private String txtCargo;
    private String txtNumLugarAds;
    private String txtDescripcion;
    private InputStream mensaje;
    private int tipoOpiniones;
    
    public OpinionController ()
    {
        opinionDAOAct = new OpinionDAOImp();
    }
    
    
    public String mostrarCentral()
    {
        return SUCCESS;
    }
    /*metodo que agrega un objeto opinion*/
    
    public String agregar() throws UnsupportedEncodingException
    {
        Date now = new Date();
        personaAct = new KPersonaOpinion(opinionAct,txtNombre,txtApellidoPat,txtApellidoMat,txtCalle,txtNumExt,txtNumInt,txtColonia,txtCp,txtPais,txtEntidadFederativa,txtDelegacion,txtEmail,txtTelefono,"red social",now,"VicCross",now,"VicCross","altaOpinios.jsp",txtEdad);
        opinionAct = new KOpinion();
        /*Exposicion*/
        CExposicion ExposicionAct = new CExposicion();
        ExposicionAct.setIdExposicion(cbEcposicion);
        opinionAct.setCExposicion(ExposicionAct);
        /*red*/
        CRedSocial redSocialAct = new CRedSocial();
        if(cbTipoOpinion == 4)
        {
            redSocialAct.setIdRed(1);
            opinionAct.setCRedSocial(redSocialAct);
        }
        else if(cbTipoOpinion == 5)
        {
            redSocialAct.setIdRed(2);
            opinionAct.setKPersonaOpinion(getPersonaAct());
        }
        
        /*tipo*/
        CTipoOpinion tipoOpinionAct = new CTipoOpinion();
        tipoOpinionAct.setIdTipo(cbTipoOpinion);
        opinionAct.setCTipoOpinion(tipoOpinionAct);
        opinionAct.setClasificacion(idTipo);
        opinionAct.setDescripcion(txtDescripcion);
        opinionAct.setFechaIngreso(txtFecha);
        opinionAct.setStatusOpinion(1);
        opinionAct.setUsuarioCreacion("VicCross");
        opinionAct.setFolio(txtFolio);
        opinionAct.setFechaCreacion(now);
        opinionAct.setUsuarioUltimaModificacion("VicCross");
        opinionAct.setFechaUltimaModificacion(now);
        opinionAct.setPantalla("altaOpiniones.jsp");
        KServidorDenunciado servidorDenunciadoAct = new KServidorDenunciado();
        servidorDenunciadoAct.setCargo(txtCargo);
        servidorDenunciadoAct.setLugar(txtNumLugarAds);
        servidorDenunciadoAct.setNombre(txtNombreServidor);
        servidorDenunciadoAct.setUsuarioCreacion("VicCross");
        servidorDenunciadoAct.setFechaCreacion(now);
        servidorDenunciadoAct.setUsuarioUltimaModificacion("VicCross");
        servidorDenunciadoAct.setFechaUltimaModificacion(now);
        servidorDenunciadoAct.setPantalla("altaOpiniones.jsp");
        opinionAct.setKPersonaOpinion(getPersonaAct());
        getPersonaAct().setKOpinion(opinionAct);
        opinionAct.setKServidorDenunciado(servidorDenunciadoAct);
        servidorDenunciadoAct.setKOpinion(opinionAct);
        opinionAct.setKCopias(null);
        opinionAct.setKRespuestas(null);
        if(opinionDAOAct.agregar(opinionAct))
        {
            mensaje = new ByteArrayInputStream("correcto".getBytes("UTF-8"));
        }
        else
        {
            mensaje = new ByteArrayInputStream("error".getBytes("UTF-8"));
        }
        return SUCCESS;
    }
    
    public String listar()
    {
        try
        {
            setOpiniones(opinionDAOAct.mostrar(this.tipoOpiniones));
            for(KOpinion opinion: opiniones)
            {
                Opinion opinionN = new Opinion ();
                opinionN.setIdOpinion(opinion.getIdOpinion());
                opinionN.setDescripcion(opinion.getDescripcion());
                opinionN.setFecha(opinion.getFechaIngreso());
                getOpinionesExt().add(opinionN);
            }
            /*for(KOpinion opinion:opiniones)
            {
                opinion.setKCopias(null);
                opinion.setKRespuestas(null);
            }*/
            //this.opiniones = null;
        }
        catch(Exception e)
        {
            setOpiniones(null);
            setOpinionesExt(null);
        }
        return SUCCESS;
    }

    /**
     * @return the opinionAct
     */
    public KOpinion getOpinionAct() {
        return opinionAct;
    }

    /**
     * @param opinionAct the opinionAct to set
     */
    public void setOpinionAct(KOpinion opinionAct) {
        this.opinionAct = opinionAct;
    }
    
    /**
     * @param opiniones the opiniones to set
     */
    public void setOpiniones(ArrayList<KOpinion> opiniones) {
        this.opiniones = opiniones;
    }

    /**
     * @return the opinionDAOAct
     */
    public OpinionDAO getOpinionDAOAct() {
        return opinionDAOAct;
    }

    /**
     * @param opinionDAOAct the opinionDAOAct to set
     */
    public void setOpinionDAOAct(OpinionDAO opinionDAOAct) {
        this.opinionDAOAct = opinionDAOAct;
    }

    /**
     * @return the cbEcposicion
     */
    public int getCbEcposicion() {
        return cbEcposicion;
    }

    /**
     * @param cbEcposicion the cbEcposicion to set
     */
    public void setCbEcposicion(int cbEcposicion) {
        this.cbEcposicion = cbEcposicion;
    }

    /**
     * @return the mensaje
     */
    public InputStream getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(InputStream mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the cbTipoOpinion
     */
    public int getCbTipoOpinion() {
        return cbTipoOpinion;
    }

    /**
     * @param cbTipoOpinion the cbTipoOpinion to set
     */
    public void setCbTipoOpinion(int cbTipoOpinion) {
        this.cbTipoOpinion = cbTipoOpinion;
    }

    /**
     * @return the idTipo
     */
    public int getIdTipo() {
        return idTipo;
    }

    /**
     * @param idTipo the idTipo to set
     */
    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    /**
     * @return the txtFolio
     */
    public String getTxtFolio() {
        return txtFolio;
    }

    /**
     * @param txtFolio the txtFolio to set
     */
    public void setTxtFolio(String txtFolio) {
        this.txtFolio = txtFolio;
    }

    /**
     * @return the txtFecha
     */
    public Date getTxtFecha() {
        return txtFecha;
    }

    /**
     * @param txtFecha the txtFecha to set
     */
    public void setTxtFecha(Date txtFecha) {
        this.txtFecha = txtFecha;
    }

    /**
     * @return the txtNombre
     */
    public String getTxtNombre() {
        return txtNombre;
    }

    /**
     * @param txtNombre the txtNombre to set
     */
    public void setTxtNombre(String txtNombre) {
        this.txtNombre = txtNombre;
    }

    /**
     * @return the txtEdad
     */
    public int getTxtEdad() {
        return txtEdad;
    }

    /**
     * @param txtEdad the txtEdad to set
     */
    public void setTxtEdad(int txtEdad) {
        this.txtEdad = txtEdad;
    }

    /**
     * @return the txtApellidoPat
     */
    public String getTxtApellidoPat() {
        return txtApellidoPat;
    }

    /**
     * @param txtApellidoPat the txtApellidoPat to set
     */
    public void setTxtApellidoPat(String txtApellidoPat) {
        this.txtApellidoPat = txtApellidoPat;
    }

    /**
     * @return the txtEmail
     */
    public String getTxtEmail() {
        return txtEmail;
    }

    /**
     * @param txtEmail the txtEmail to set
     */
    public void setTxtEmail(String txtEmail) {
        this.txtEmail = txtEmail;
    }

    /**
     * @return the txtApellidoMat
     */
    public String getTxtApellidoMat() {
        return txtApellidoMat;
    }

    /**
     * @param txtApellidoMat the txtApellidoMat to set
     */
    public void setTxtApellidoMat(String txtApellidoMat) {
        this.txtApellidoMat = txtApellidoMat;
    }

    /**
     * @return the txtTelefono
     */
    public String getTxtTelefono() {
        return txtTelefono;
    }

    /**
     * @param txtTelefono the txtTelefono to set
     */
    public void setTxtTelefono(String txtTelefono) {
        this.txtTelefono = txtTelefono;
    }

    /**
     * @return the txtPais
     */
    public String getTxtPais() {
        return txtPais;
    }

    /**
     * @param txtPais the txtPais to set
     */
    public void setTxtPais(String txtPais) {
        this.txtPais = txtPais;
    }

    /**
     * @return the txtEntidadFederativa
     */
    public String getTxtEntidadFederativa() {
        return txtEntidadFederativa;
    }

    /**
     * @param txtEntidadFederativa the txtEntidadFederativa to set
     */
    public void setTxtEntidadFederativa(String txtEntidadFederativa) {
        this.txtEntidadFederativa = txtEntidadFederativa;
    }

    /**
     * @return the txtDelegacion
     */
    public String getTxtDelegacion() {
        return txtDelegacion;
    }

    /**
     * @param txtDelegacion the txtDelegacion to set
     */
    public void setTxtDelegacion(String txtDelegacion) {
        this.txtDelegacion = txtDelegacion;
    }

    /**
     * @return the txtColonia
     */
    public String getTxtColonia() {
        return txtColonia;
    }

    /**
     * @param txtColonia the txtColonia to set
     */
    public void setTxtColonia(String txtColonia) {
        this.txtColonia = txtColonia;
    }

    /**
     * @return the txtCalle
     */
    public String getTxtCalle() {
        return txtCalle;
    }

    /**
     * @param txtCalle the txtCalle to set
     */
    public void setTxtCalle(String txtCalle) {
        this.txtCalle = txtCalle;
    }

    /**
     * @return the txtNumExt
     */
    public String getTxtNumExt() {
        return txtNumExt;
    }

    /**
     * @param txtNumExt the txtNumExt to set
     */
    public void setTxtNumExt(String txtNumExt) {
        this.txtNumExt = txtNumExt;
    }

    /**
     * @return the txtCp
     */
    public String getTxtCp() {
        return txtCp;
    }

    /**
     * @param txtCp the txtCp to set
     */
    public void setTxtCp(String txtCp) {
        this.txtCp = txtCp;
    }

    /**
     * @return the txtNumInt
     */
    public String getTxtNumInt() {
        return txtNumInt;
    }

    /**
     * @param txtNumInt the txtNumInt to set
     */
    public void setTxtNumInt(String txtNumInt) {
        this.txtNumInt = txtNumInt;
    }

    /**
     * @return the txtNombreServidor
     */
    public String getTxtNombreServidor() {
        return txtNombreServidor;
    }

    /**
     * @param txtNombreServidor the txtNombreServidor to set
     */
    public void setTxtNombreServidor(String txtNombreServidor) {
        this.txtNombreServidor = txtNombreServidor;
    }

    /**
     * @return the txtCargo
     */
    public String getTxtCargo() {
        return txtCargo;
    }

    /**
     * @param txtCargo the txtCargo to set
     */
    public void setTxtCargo(String txtCargo) {
        this.txtCargo = txtCargo;
    }

    /**
     * @return the txtNumLugarAds
     */
    public String getTxtNumLugarAds() {
        return txtNumLugarAds;
    }

    /**
     * @param txtNumLugarAds the txtNumLugarAds to set
     */
    public void setTxtNumLugarAds(String txtNumLugarAds) {
        this.txtNumLugarAds = txtNumLugarAds;
    }

    /**
     * @return the txtDescripcion
     */
    public String getTxtDescripcion() {
        return txtDescripcion;
    }

    /**
     * @param txtDescripcion the txtDescripcion to set
     */
    public void setTxtDescripcion(String txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    /**
     * @return the txtUrl
     */
    public String getTxtUrl() {
        return txtUrl;
    }

    /**
     * @param txtUrl the txtUrl to set
     */
    public void setTxtUrl(String txtUrl) {
        this.txtUrl = txtUrl;
    }

    /**
     * @return the tipoOpiniones
     */
    public int getTipoOpiniones() {
        return tipoOpiniones;
    }

    /**
     * @param tipoOpiniones the tipoOpiniones to set
     */
    public void setTipoOpiniones(int tipoOpiniones) {
        this.tipoOpiniones = tipoOpiniones;
    }

    /**
     * @return the personaAct
     */
    public KPersonaOpinion getPersonaAct() {
        return personaAct;
    }

    /**
     * @return the opinionesExt
     */
    public ArrayList<Opinion> getOpinionesExt() {
        return opinionesExt;
    }

    /**
     * @param opinionesExt the opinionesExt to set
     */
    public void setOpinionesExt(ArrayList<Opinion> opinionesExt) {
        this.opinionesExt = opinionesExt;
    }

}
