/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Date;

/**
 *
 * @author VicCross
 */
public class Opinion {
    
    private int idOpinion;
    private String descripcion;
    private Date fecha;

    /**
     * @return the idOpinion
     */
    public int getIdOpinion() {
        return idOpinion;
    }

    /**
     * @param idOpinion the idOpinion to set
     */
    public void setIdOpinion(int idOpinion) {
        this.idOpinion = idOpinion;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
}
