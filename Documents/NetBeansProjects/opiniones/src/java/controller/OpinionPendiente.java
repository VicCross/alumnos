/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;

/**
 *
 * @author VicCross
 */
public class OpinionPendiente {
    private String nombreConcepto;
    private int idOpinion;
    private ArrayList <Opinion> opiniones;

    /**
     * @return the nombreConcepto
     */
    public String getNombreConcepto() {
        return nombreConcepto;
    }

    /**
     * @param nombreConcepto the nombreConcepto to set
     */
    public void setNombreConcepto(String nombreConcepto) {
        this.nombreConcepto = nombreConcepto;
    }

    /**
     * @return the idOpinion
     */
    public int getIdOpinion() {
        return idOpinion;
    }

    /**
     * @param idOpinion the idOpinion to set
     */
    public void setIdOpinion(int idOpinion) {
        this.idOpinion = idOpinion;
    }

    /**
     * @return the opiniones
     */
    public ArrayList <Opinion> getOpiniones() {
        return opiniones;
    }

    /**
     * @param opiniones the opiniones to set
     */
    public void setOpiniones(ArrayList <Opinion> opiniones) {
        this.opiniones = opiniones;
    }
    
}
