/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import model.DAO.ConceptoDAOImp;
import model.entities.CConcepto;
import model.entities.KOpinion;

/**
 *
 * @author VicCross
 */
public class PendientesController extends ActionSupport
{
    private ArrayList <OpinionPendiente> opiniones = new ArrayList <>();
    private String area;
    private int clasificacion;
    private String clasif;
    private String cadenaAct;
    private int idArea;
    public String mostrarPendientes()
    {
        switch (clasificacion)
        {
                case 1:
                    clasif = "Felicitaciones";
                    break;
                case 2:
                    clasif = "Peticiones";
                    break;
                case 3:
                    clasif = "Sugerencias";
                    break;
                case 4:
                    clasif = "Quejas";
                    break;
        }
        ConceptoDAOImp conceptoDAOAct = new ConceptoDAOImp ();
        ArrayList <CConcepto> conceptos = new ArrayList <>();
        conceptos = conceptoDAOAct.listarActividadesPendientes(idArea,clasificacion);
        for (CConcepto conceptoAct: conceptos)
        {
            if (conceptoAct.getKOpinions().size()>0)
            {
                OpinionPendiente opinionAct = new OpinionPendiente ();
                opinionAct.setNombreConcepto("");
                opinionAct.setOpiniones(new ArrayList<>());
                cadenaAct = "";
                generarNombreConcepto (conceptoAct);
                opinionAct.setNombreConcepto(cadenaAct);
                for(Object objetoAct: conceptoAct.getKOpinions())
                {
                    KOpinion opinionModelAct = new KOpinion();
                    opinionModelAct = (KOpinion) objetoAct;
                    Opinion opinionPlana = new Opinion();
                    opinionPlana.setIdOpinion(opinionModelAct.getIdOpinion());
                    opinionPlana.setDescripcion(opinionModelAct.getDescripcion());
                    opinionPlana.setFecha(opinionModelAct.getFechaIngreso());
                    opinionAct.getOpiniones().add(opinionPlana);
                }
                getOpiniones().add(opinionAct);
            }
        }
        return SUCCESS;
    }
    
    private void generarNombreConcepto(CConcepto conceptoAct)
    {
        if(!"".equals(cadenaAct))
        {
            cadenaAct = conceptoAct.getNombre() + " > " + cadenaAct;
        }
        else
        {
            cadenaAct = conceptoAct.getNombre();
        }
        if(conceptoAct.getCConcepto() != null)
        {
            generarNombreConcepto(conceptoAct.getCConcepto());
        }
    }

    /**
     * @return the opiniones
     */
    public ArrayList <OpinionPendiente> getOpiniones() {
        return opiniones;
    }

    /**
     * @param opiniones the opiniones to set
     */
    public void setOpiniones(ArrayList <OpinionPendiente> opiniones) {
        this.opiniones = opiniones;
    }

    /**
     * @return the area
     */
    public String getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * @param clasificacion the clasificacion to set
     */
    public void setClasificacion(int clasificacion) {
        this.clasificacion = clasificacion;
    }

    /**
     * @return the clasif
     */
    public String getClasif() {
        return clasif;
    }

    /**
     * @param idArea the idArea to set
     */
    public void setIdArea(int idArea) {
        this.idArea = idArea;
    }
}
