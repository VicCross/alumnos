/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import model.DAO.OpinionDAOImp;
import model.entities.KOpinion;

/**
 *
 * @author VicCross
 */
public class MostrarOpinionDerivController extends ActionSupport{
    
    private int idOpinion;
    private KOpinion opinionAct;
    private OpinionDAOImp opinionDAO = new OpinionDAOImp();
    
    public String consultarOpinion()
    {
        try
        {
            opinionAct = new KOpinion();
            opinionAct = opinionDAO.consultarDeriv(idOpinion);
            return SUCCESS;
        }
        catch(Exception e)
        {
            return ERROR;
        }
    }

    /**
     * @param idOpinion the idOpinion to set
     */
    public void setIdOpinion(int idOpinion) {
        this.idOpinion = idOpinion;
    }

    /**
     * @return the opinionAct
     */
    public KOpinion getOpinionAct() {
        return opinionAct;
    }
    
}
