package model.entities;
// Generated 13/09/2016 05:18:03 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * CGrupoTrabajo generated by hbm2java
 */
public class CGrupoTrabajo  implements java.io.Serializable {


     private Integer idGrupoTrabajo;
     private String nombre;
     private String descripcion;
     private Date fechaCreacion;
     private String usuarioCreacion;
     private Date fechaUltimaModificacion;
     private String usuarioUltimaModificacion;
     private String pantalla;

    public CGrupoTrabajo() {
    }

	
    public CGrupoTrabajo(String nombre, Date fechaCreacion, String usuarioCreacion, Date fechaUltimaModificacion, String usuarioUltimaModificacion, String pantalla) {
        this.nombre = nombre;
        this.fechaCreacion = fechaCreacion;
        this.usuarioCreacion = usuarioCreacion;
        this.fechaUltimaModificacion = fechaUltimaModificacion;
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
        this.pantalla = pantalla;
    }
    public CGrupoTrabajo(String nombre, String descripcion, Date fechaCreacion, String usuarioCreacion, Date fechaUltimaModificacion, String usuarioUltimaModificacion, String pantalla) {
       this.nombre = nombre;
       this.descripcion = descripcion;
       this.fechaCreacion = fechaCreacion;
       this.usuarioCreacion = usuarioCreacion;
       this.fechaUltimaModificacion = fechaUltimaModificacion;
       this.usuarioUltimaModificacion = usuarioUltimaModificacion;
       this.pantalla = pantalla;
    }
   
    public Integer getIdGrupoTrabajo() {
        return this.idGrupoTrabajo;
    }
    
    public void setIdGrupoTrabajo(Integer idGrupoTrabajo) {
        this.idGrupoTrabajo = idGrupoTrabajo;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    public String getUsuarioCreacion() {
        return this.usuarioCreacion;
    }
    
    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }
    public Date getFechaUltimaModificacion() {
        return this.fechaUltimaModificacion;
    }
    
    public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }
    public String getUsuarioUltimaModificacion() {
        return this.usuarioUltimaModificacion;
    }
    
    public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
    }
    public String getPantalla() {
        return this.pantalla;
    }
    
    public void setPantalla(String pantalla) {
        this.pantalla = pantalla;
    }




}


