package model.entities;
// Generated 13/09/2016 05:18:03 PM by Hibernate Tools 4.3.1



/**
 * CDelegacion generated by hbm2java
 */
public class CDelegacion  implements java.io.Serializable {


     private Integer idMunicipio;
     private String nombreMunicipio;
     private int estado;

    public CDelegacion() {
    }

    public CDelegacion(String nombreMunicipio, int estado) {
       this.nombreMunicipio = nombreMunicipio;
       this.estado = estado;
    }
   
    public Integer getIdMunicipio() {
        return this.idMunicipio;
    }
    
    public void setIdMunicipio(Integer idMunicipio) {
        this.idMunicipio = idMunicipio;
    }
    public String getNombreMunicipio() {
        return this.nombreMunicipio;
    }
    
    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }
    public int getEstado() {
        return this.estado;
    }
    
    public void setEstado(int estado) {
        this.estado = estado;
    }




}


