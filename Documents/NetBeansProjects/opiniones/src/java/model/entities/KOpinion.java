package model.entities;
// Generated 13/09/2016 05:18:03 PM by Hibernate Tools 4.3.1


import model.entities.CExposicion;
import model.entities.CConcepto;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * KOpinion generated by hbm2java
 */
public class KOpinion  implements java.io.Serializable {


     private Integer idOpinion;
     private CConcepto CConcepto;
     private CExposicion CExposicion;
     private CRedSocial CRedSocial;
     private CTipoOpinion CTipoOpinion;
     private int statusOpinion;
     private int clasificacion;
     private Date fechaIngreso;
     private Date fechaTurno;
     private String descripcion;
     private Date fechaCreacion;
     private String usuarioCreacion;
     private Date fechaUltimaModificacion;
     private String usuarioUltimaModificacion;
     private String pantalla;
     private String folio;
     private KPersonaOpinion KPersonaOpinion;
     private Set KCopias = new HashSet(0);
     private KServidorDenunciado KServidorDenunciado;
     private Set KRespuestas = new HashSet(0);

    public KOpinion() {
    }

	
    public KOpinion(CTipoOpinion CTipoOpinion, int statusOpinion, int clasificacion) {
        this.CTipoOpinion = CTipoOpinion;
        this.statusOpinion = statusOpinion;
        this.clasificacion = clasificacion;
    }
    public KOpinion(CConcepto CConcepto, CExposicion CExposicion, CRedSocial CRedSocial, CTipoOpinion CTipoOpinion, int statusOpinion, int clasificacion, Date fechaIngreso, Date fechaTurno, String descripcion, Date fechaCreacion, String usuarioCreacion, Date fechaUltimaModificacion, String usuarioUltimaModificacion, String pantalla, String folio, KPersonaOpinion KPersonaOpinion, Set KCopias, KServidorDenunciado KServidorDenunciado, Set KRespuestas) {
       this.CConcepto = CConcepto;
       this.CExposicion = CExposicion;
       this.CRedSocial = CRedSocial;
       this.CTipoOpinion = CTipoOpinion;
       this.statusOpinion = statusOpinion;
       this.clasificacion = clasificacion;
       this.fechaIngreso = fechaIngreso;
       this.fechaTurno = fechaTurno;
       this.descripcion = descripcion;
       this.fechaCreacion = fechaCreacion;
       this.usuarioCreacion = usuarioCreacion;
       this.fechaUltimaModificacion = fechaUltimaModificacion;
       this.usuarioUltimaModificacion = usuarioUltimaModificacion;
       this.pantalla = pantalla;
       this.folio = folio;
       this.KPersonaOpinion = KPersonaOpinion;
       this.KCopias = KCopias;
       this.KServidorDenunciado = KServidorDenunciado;
       this.KRespuestas = KRespuestas;
    }
    
    public KOpinion(int idOpinion, String descripcion, Date fechaIngreso) {
        this.idOpinion = idOpinion;
        this.descripcion = descripcion;
        this.fechaIngreso = fechaIngreso;
    }
   
    public Integer getIdOpinion() {
        return this.idOpinion;
    }
    
    public void setIdOpinion(Integer idOpinion) {
        this.idOpinion = idOpinion;
    }
    public CConcepto getCConcepto() {
        return this.CConcepto;
    }
    
    public void setCConcepto(CConcepto CConcepto) {
        this.CConcepto = CConcepto;
    }
    public CExposicion getCExposicion() {
        return this.CExposicion;
    }
    
    public void setCExposicion(CExposicion CExposicion) {
        this.CExposicion = CExposicion;
    }
    public CRedSocial getCRedSocial() {
        return this.CRedSocial;
    }
    
    public void setCRedSocial(CRedSocial CRedSocial) {
        this.CRedSocial = CRedSocial;
    }
    public CTipoOpinion getCTipoOpinion() {
        return this.CTipoOpinion;
    }
    
    public void setCTipoOpinion(CTipoOpinion CTipoOpinion) {
        this.CTipoOpinion = CTipoOpinion;
    }
    public int getStatusOpinion() {
        return this.statusOpinion;
    }
    
    public void setStatusOpinion(int statusOpinion) {
        this.statusOpinion = statusOpinion;
    }
    public int getClasificacion() {
        return this.clasificacion;
    }
    
    public void setClasificacion(int clasificacion) {
        this.clasificacion = clasificacion;
    }
    public Date getFechaIngreso() {
        return this.fechaIngreso;
    }
    
    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
    public Date getFechaTurno() {
        return this.fechaTurno;
    }
    
    public void setFechaTurno(Date fechaTurno) {
        this.fechaTurno = fechaTurno;
    }
    public String getDescripcion() {
        return this.descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    public String getUsuarioCreacion() {
        return this.usuarioCreacion;
    }
    
    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }
    public Date getFechaUltimaModificacion() {
        return this.fechaUltimaModificacion;
    }
    
    public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }
    public String getUsuarioUltimaModificacion() {
        return this.usuarioUltimaModificacion;
    }
    
    public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
    }
    public String getPantalla() {
        return this.pantalla;
    }
    
    public void setPantalla(String pantalla) {
        this.pantalla = pantalla;
    }
    public String getFolio() {
        return this.folio;
    }
    
    public void setFolio(String folio) {
        this.folio = folio;
    }
    public KPersonaOpinion getKPersonaOpinion() {
        return this.KPersonaOpinion;
    }
    
    public void setKPersonaOpinion(KPersonaOpinion KPersonaOpinion) {
        this.KPersonaOpinion = KPersonaOpinion;
    }
    public Set getKCopias() {
        return this.KCopias;
    }
    
    public void setKCopias(Set KCopias) {
        this.KCopias = KCopias;
    }
    public KServidorDenunciado getKServidorDenunciado() {
        return this.KServidorDenunciado;
    }
    
    public void setKServidorDenunciado(KServidorDenunciado KServidorDenunciado) {
        this.KServidorDenunciado = KServidorDenunciado;
    }
    public Set getKRespuestas() {
        return this.KRespuestas;
    }
    
    public void setKRespuestas(Set KRespuestas) {
        this.KRespuestas = KRespuestas;
    }




}


