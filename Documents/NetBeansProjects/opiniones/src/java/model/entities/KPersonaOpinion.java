package model.entities;
// Generated 13/09/2016 05:18:03 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * KPersonaOpinion generated by hbm2java
 */
public class KPersonaOpinion  implements java.io.Serializable {


     private Integer idOpinion;
     private KOpinion KOpinion;
     private String nombre;
     private String apellidoPaterno;
     private String apellidoMaterno;
     private String calle;
     private String noExt;
     private String noInt;
     private String colonia;
     private String codigoPostal;
     private String nacionalidad;
     private String estado;
     private String delegacion;
     private String email;
     private String telefono;
     private String perfilRedSocial;
     private Date fechaCreacion;
     private String usuarioCreacion;
     private Date fechaUltimaModificacion;
     private String usuarioUltimaModificacion;
     private String pantalla;
     private Integer edad;

    public KPersonaOpinion() {
    }

	
    public KPersonaOpinion(KOpinion KOpinion) {
        this.KOpinion = KOpinion;
    }
    public KPersonaOpinion(KOpinion KOpinion, String nombre, String apellidoPaterno, String apellidoMaterno, String calle, String noExt, String noInt, String colonia, String codigoPostal, String nacionalidad, String estado, String delegacion, String email, String telefono, String perfilRedSocial, Date fechaCreacion, String usuarioCreacion, Date fechaUltimaModificacion, String usuarioUltimaModificacion, String pantalla, Integer edad) {
       this.KOpinion = KOpinion;
       this.nombre = nombre;
       this.apellidoPaterno = apellidoPaterno;
       this.apellidoMaterno = apellidoMaterno;
       this.calle = calle;
       this.noExt = noExt;
       this.noInt = noInt;
       this.colonia = colonia;
       this.codigoPostal = codigoPostal;
       this.nacionalidad = nacionalidad;
       this.estado = estado;
       this.delegacion = delegacion;
       this.email = email;
       this.telefono = telefono;
       this.perfilRedSocial = perfilRedSocial;
       this.fechaCreacion = fechaCreacion;
       this.usuarioCreacion = usuarioCreacion;
       this.fechaUltimaModificacion = fechaUltimaModificacion;
       this.usuarioUltimaModificacion = usuarioUltimaModificacion;
       this.pantalla = pantalla;
       this.edad = edad;
    }
   
    public Integer getIdOpinion() {
        return this.idOpinion;
    }
    
    public void setIdOpinion(Integer idOpinion) {
        this.idOpinion = idOpinion;
    }
    public KOpinion getKOpinion() {
        return this.KOpinion;
    }
    
    public void setKOpinion(KOpinion KOpinion) {
        this.KOpinion = KOpinion;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellidoPaterno() {
        return this.apellidoPaterno;
    }
    
    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }
    public String getApellidoMaterno() {
        return this.apellidoMaterno;
    }
    
    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }
    public String getCalle() {
        return this.calle;
    }
    
    public void setCalle(String calle) {
        this.calle = calle;
    }
    public String getNoExt() {
        return this.noExt;
    }
    
    public void setNoExt(String noExt) {
        this.noExt = noExt;
    }
    public String getNoInt() {
        return this.noInt;
    }
    
    public void setNoInt(String noInt) {
        this.noInt = noInt;
    }
    public String getColonia() {
        return this.colonia;
    }
    
    public void setColonia(String colonia) {
        this.colonia = colonia;
    }
    public String getCodigoPostal() {
        return this.codigoPostal;
    }
    
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
    public String getNacionalidad() {
        return this.nacionalidad;
    }
    
    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }
    public String getEstado() {
        return this.estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
    public String getDelegacion() {
        return this.delegacion;
    }
    
    public void setDelegacion(String delegacion) {
        this.delegacion = delegacion;
    }
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    public String getTelefono() {
        return this.telefono;
    }
    
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getPerfilRedSocial() {
        return this.perfilRedSocial;
    }
    
    public void setPerfilRedSocial(String perfilRedSocial) {
        this.perfilRedSocial = perfilRedSocial;
    }
    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    public String getUsuarioCreacion() {
        return this.usuarioCreacion;
    }
    
    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }
    public Date getFechaUltimaModificacion() {
        return this.fechaUltimaModificacion;
    }
    
    public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }
    public String getUsuarioUltimaModificacion() {
        return this.usuarioUltimaModificacion;
    }
    
    public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
    }
    public String getPantalla() {
        return this.pantalla;
    }
    
    public void setPantalla(String pantalla) {
        this.pantalla = pantalla;
    }
    public Integer getEdad() {
        return this.edad;
    }
    
    public void setEdad(Integer edad) {
        this.edad = edad;
    }




}


