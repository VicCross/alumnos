/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import model.DAO.interfaces.ConceptoDAO;
import model.entities.CConcepto;
import model.entities.KOpinion;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author VicCross
 */
public class ConceptoDAOImp implements ConceptoDAO {

    Transaction transaccion;
    Session sesion;
    
    @Override
    public ArrayList<CConcepto> listar() {
        try
        {
            sesion = HibernateUtil.abrirSesion();
            transaccion = sesion.beginTransaction();
            ArrayList<CConcepto> conceptos = new ArrayList<>();
            String query = "from CConcepto";
            conceptos = (ArrayList<CConcepto>) sesion.createQuery(query).list();
            transaccion.commit();
            sesion.close();
            return conceptos;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return null;
        }
    }

    @Override
    public ArrayList<CConcepto> listarArea(int idArea) {
        try
        {
            sesion = HibernateUtil.abrirSesion();
            transaccion = sesion.beginTransaction();
            ArrayList<CConcepto> conceptos = new ArrayList<>();
            String query = "Select c FROM CConcepto c " +
            "WHERE c.CTipoConcepto.idTipoConcepto = 1 AND c.CArea.idArea = " + idArea +
            " GROUP BY c.idConcepto ORDER BY c.CConcepto.idConcepto ASC NULLS FIRST";        
            conceptos = (ArrayList<CConcepto>) sesion.createQuery(query).list();
            transaccion.commit();
            sesion.close();
            return conceptos;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return null;
        }
    }

    @Override
    public ArrayList<CConcepto> listarProyecto(int idProyecto)
    {
        try
        {
            sesion = HibernateUtil.abrirSesion();
            transaccion = sesion.beginTransaction();
            ArrayList<CConcepto> conceptos = new ArrayList<>();
            String query ="Select c FROM CConcepto c left join c.KConceptoProyectos cp " +
            "WHERE c.CTipoConcepto.idTipoConcepto = 2 AND cp.CProyecto.idProyecto = " + idProyecto +
            " GROUP BY c.idConcepto ORDER BY c.CConcepto.idConcepto ASC NULLS FIRST";    
            conceptos = (ArrayList<CConcepto>) sesion.createQuery(query).list();
            transaccion.commit();
            sesion.close();
            return conceptos;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return null;
        }
    }

    @Override
    public ArrayList<CConcepto> listarActividadesPendientes(int idArea, int clasificacion)
    {
        try
        {
            sesion = HibernateUtil.abrirSesion();
            transaccion = sesion.beginTransaction();
            ArrayList<CConcepto> conceptos = new ArrayList<>();
            String query = "from CConcepto a where a.CArea.idArea = " + idArea + " and a.KOpinions is not empty";
            //query = "select new CConcepto(a.idConcepto,a.CConcepto, a.nombre) from CConcepto a join a.KOpinions b where b.statusOpinion = 3 and a.CArea.idArea = 5 group by a.idConcepto";
            conceptos = (ArrayList<CConcepto>) sesion.createQuery(query).list();
            for(CConcepto conceptoAct: conceptos)
            {
                query = "from KOpinion where clasificacion = "+ clasificacion + " and statusOpinion in (3,4) and CConcepto.idConcepto = " + conceptoAct.getIdConcepto();
                ArrayList <KOpinion> opiniones = (ArrayList<KOpinion>) sesion.createQuery(query).list();
                conceptoAct.setKOpinions(new HashSet (opiniones));
            }
            transaccion.commit();
            sesion.close();
            return conceptos;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return null;
        }
    }
    
}
