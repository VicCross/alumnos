/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import model.DAO.interfaces.ExposicionDAO;
import java.util.ArrayList;
import model.entities.CExposicion;
import org.hibernate.Transaction;
import org.hibernate.Session;

/**
 *
 * @author VicCross
 */
public class ExposicionDAOImp implements ExposicionDAO {

    Transaction transaccion;
    Session sesion;
    
    @Override
    public ArrayList<CExposicion> mostrar() {
        try
        {
            sesion = HibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            ArrayList<CExposicion> exposiciones = (ArrayList<CExposicion>) sesion.createQuery("from CExposicion").list();
            sesion.close();
            return exposiciones;
        }
        catch(Exception e)
        {
            System.out.println(e);
            return null;
            
        }
    }
    
}
