/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import java.util.ArrayList;
import model.DAO.interfaces.PaisDAO;
import model.entities.CPais;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author VicCross
 */
public class PaisDAOImp implements PaisDAO {
    Transaction transaccion;
    Session sesion;

    @Override
    public ArrayList<CPais> listar() {
        try
        {
            sesion = HibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            ArrayList<CPais> paises = (ArrayList<CPais>) sesion.createQuery("from CPais").list();
            sesion.close();
            return paises;
        }
        catch(Exception e)
        {
            return null;
        }
    }
    
}
