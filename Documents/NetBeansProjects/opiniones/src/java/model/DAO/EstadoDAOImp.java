/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import java.util.ArrayList;
import model.DAO.interfaces.EstadoDAO;
import model.entities.CEstado;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author VicCross
 */
public class EstadoDAOImp implements EstadoDAO {
    Transaction transaccion;
    Session sesion;
    
    @Override
    public ArrayList<CEstado> listar() {
        try
        {
            sesion = HibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            ArrayList<CEstado> estados = (ArrayList<CEstado>) sesion.createQuery("from CEstado").list();
            sesion.close();
            return estados;
        }
        catch(Exception e)
        {
            return null;
        }
    }

    @Override
    public ArrayList<CEstado> listar(int idPais) {
        try
        {
            sesion = HibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            String query = "from CEstado where ubicacionpaisid = " + Integer.toString(idPais);
            ArrayList<CEstado> estados = (ArrayList<CEstado>) sesion.createQuery(query).list();
            sesion.close();
            return estados;
        }
        catch(Exception e)
        {
            return null;
        }
    }
    
}
