/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import model.entities.CProyecto;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author VicCross
 */
public class ProyectoDAOImp extends ActionSupport 
{
    Transaction transaccion;
    Session sesion;
    
    public ArrayList<CProyecto> listar()
    {
        try
        {
            sesion = HibernateUtil.abrirSesion();
            transaccion = sesion.beginTransaction();
            ArrayList<CProyecto> proyectos = new ArrayList<>();
            String query = "from CProyecto";
            proyectos = (ArrayList<CProyecto>) sesion.createQuery(query).list();
            transaccion.commit();
            sesion.close();
            return proyectos;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return null;
        }
    }
}
