/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import java.util.ArrayList;
import model.DAO.interfaces.DelegacionDAO;
import model.entities.CDelegacion;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author VicCross
 */
public class DelegacionDAOImp implements DelegacionDAO {
    Transaction transaccion;
    Session sesion;
    
    @Override
    public ArrayList<CDelegacion> listar() {
        try
        {
            sesion = HibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            ArrayList<CDelegacion> delegaciones = (ArrayList<CDelegacion>) sesion.createQuery("from CDelegacion").list();
            sesion.close();
            HibernateUtil.getSessionFactory().close();
            return delegaciones;
        }
        catch(Exception e)
        {
            return null;
        }
    }
    
    @Override
    public ArrayList<CDelegacion> listar(int estado) {
        try
        {
            sesion = HibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            String query = "from CDelegacion where Estado =" + Integer.toString(estado);
            ArrayList<CDelegacion> delegaciones = (ArrayList<CDelegacion>) sesion.createQuery(query).list();
            sesion.close();
            return delegaciones;
        }
        catch(Exception e)
        {
            return null;
        }
    }
}
