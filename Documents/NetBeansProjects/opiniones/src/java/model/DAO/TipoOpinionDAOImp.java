/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import java.util.ArrayList;
import model.DAO.interfaces.TipoOpinionDAO;
import model.entities.CTipoOpinion;
import org.hibernate.Transaction;
import org.hibernate.Session;

/**
 *
 * @author VicCross
 */
public class TipoOpinionDAOImp implements TipoOpinionDAO{

    Transaction transaccion;
    Session sesion;
    @Override
    public ArrayList<CTipoOpinion> listar() {
        try
        {
            sesion = HibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            ArrayList<CTipoOpinion> tipos = (ArrayList<CTipoOpinion>) sesion.createQuery("from CTipoOpinion").list();
            sesion.close();
            return tipos;
        }
        catch(Exception e)
        {
            return null;
        }
    }
    
}
