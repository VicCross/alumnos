/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import java.util.ArrayList;
import model.DAO.interfaces.AreaDAO;
import model.entities.CArea;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author VicCross
 */
public class AreaDAOImp implements AreaDAO
{
    Transaction transaccion;
    Session sesion;
    
    public ArrayList<CArea> listar()
    {
        try
        {
            sesion = HibernateUtil.abrirSesion();
            transaccion = sesion.beginTransaction();
            ArrayList<CArea> areas = new ArrayList<>();
            String query = "from CArea";
            areas = (ArrayList<CArea>) sesion.createQuery(query).list();
            transaccion.commit();
            sesion.close();
            return areas;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return null;
        }
    }

    @Override
    public CArea consultar(int idArea) {
        try
        {
            sesion = HibernateUtil.abrirSesion();
            transaccion = sesion.beginTransaction();
            CArea areaAct = null;
            areaAct = (CArea) sesion.get(CArea.class, idArea);
            transaccion.commit();
            sesion.close();
            return areaAct;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return null;
        }
    }

    @Override
    public String consultarArea(int idArea) 
    {
        String resultado = "";
        try
        {
            sesion = HibernateUtil.abrirSesion();
            transaccion = sesion.beginTransaction();
            resultado = sesion.createQuery("select nombre from CArea where idArea = " + idArea).uniqueResult().toString();
            transaccion.commit();
            sesion.close();
        }
        catch(Exception e)
        {
            resultado = "";
        }
        return resultado;
    }
    
}
