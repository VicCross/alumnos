/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO.interfaces;

import java.util.ArrayList;
import model.entities.CConcepto;

/**
 *
 * @author VicCross
 */
public interface ConceptoDAO
{
    ArrayList<CConcepto> listar ();
    ArrayList<CConcepto> listarArea (int idArea);
    ArrayList<CConcepto> listarProyecto (int idProyecto);
    ArrayList<CConcepto> listarActividadesPendientes (int idArea, int clasificacion);
}
