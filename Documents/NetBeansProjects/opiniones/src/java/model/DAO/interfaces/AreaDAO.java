/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO.interfaces;

import java.util.ArrayList;
import model.entities.CArea;

/**
 *
 * @author VicCross
 */
public interface AreaDAO
{
    public ArrayList<CArea> listar ();
    public CArea consultar(int idArea);
    public String consultarArea (int idArea);
}
