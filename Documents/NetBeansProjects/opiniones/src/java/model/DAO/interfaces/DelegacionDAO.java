/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO.interfaces;

import java.util.ArrayList;
import model.entities.CDelegacion;

/**
 *
 * @author VicCross
 */
public interface DelegacionDAO {
    public ArrayList<CDelegacion> listar ();
    public ArrayList<CDelegacion> listar (int idEstado);
}
