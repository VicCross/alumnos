/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO.interfaces;

import controller.Clasificacion;
import model.entities.KOpinion;
import java.util.*;
import model.entities.KCopia;

/**
 *
 * @author Home
 */
public interface OpinionDAO {
    public boolean agregar(KOpinion opinionAct);
    public ArrayList<KOpinion> mostrar(int tipoOpinion);
    public KOpinion consultar(int idOpinion);
     public KOpinion consultarDeriv(int idOpinion);
    public boolean modificar(KOpinion opinionAct);
    public boolean turnar(KOpinion opinionAct); //probablemente reciba el id y ya en la implementacion lo construyo y lo guardo
    public boolean guardarCopias (Set <KCopia> KCopias);
    public boolean finalizar (int[] opiniones);
    public ArrayList<Clasificacion> consultarClasificacion(int idArea);
}
