/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO.interfaces;

import java.util.*;
import model.entities.CTipoOpinion;
/**
 *
 * @author VicCross
 */
public interface TipoOpinionDAO {
    public ArrayList<CTipoOpinion> listar ();
}
