/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.DAO;

import controller.Clasificacion;
import model.DAO.interfaces.OpinionDAO;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import model.entities.KCopia;
import model.entities.KOpinion;
import org.hibernate.Transaction;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author Home
 */
public class OpinionDAOImp implements OpinionDAO{
    Transaction transaccion;
    Session sesion;

    @Override
    public KOpinion consultar(int idOpinion)
    {
        try
        {
            sesion = HibernateUtil.abrirSesion();
            transaccion = sesion.beginTransaction();
            KOpinion opinionAct = null;
            String query = "from KOpinion where id_opinion = " + idOpinion;
            opinionAct = (KOpinion) sesion.get(KOpinion.class, idOpinion);
            transaccion.commit();
            transaccion = sesion.beginTransaction();
            opinionAct.setStatusOpinion(2);
            sesion.update(opinionAct);
            transaccion.commit();
            sesion.close();
            return opinionAct;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return null;
        }
        
    }
    
    @Override
    public KOpinion consultarDeriv(int idOpinion)
    {
        try
        {
            sesion = HibernateUtil.abrirSesion();
            transaccion = sesion.beginTransaction();
            KOpinion opinionAct = null;
            String query = "from KOpinion where id_opinion = " + idOpinion;
            opinionAct = (KOpinion) sesion.get(KOpinion.class, idOpinion);
            transaccion.commit();
            transaccion = sesion.beginTransaction();
            opinionAct.setStatusOpinion(4);
            sesion.update(opinionAct);
            transaccion.commit();
            sesion.close();
            return opinionAct;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return null;
        }
        
    }
    
    @Override
    public boolean agregar(KOpinion opinionAct) {
        try
        {
            //construir los objetos que requiere el concepto 
            sesion = HibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            sesion.save(opinionAct);
            sesion.save(opinionAct.getKPersonaOpinion());
            sesion.save(opinionAct.getKServidorDenunciado());
            transaccion.commit();
            sesion.close();
            return true;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return false;
        }
    }
    
    @Override
    public boolean modificar(KOpinion opinionAct) {
        try
        {
            //construir los objetos que requiere el concepto 
            sesion = HibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            sesion.update(opinionAct);
            transaccion.commit();
            sesion.flush();
            sesion.close();
            return true;
        }
        catch(ConstraintViolationException e)
        {
            e.printStackTrace();
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return false;
        }
    }

    @Override
    public ArrayList<KOpinion> mostrar(int tipoOpinion) {
        try
        {
            sesion = HibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            String query;
            if (tipoOpinion == 0)
            {
                query = "select new KOpinion(a.idOpinion, a.descripcion,a.fechaIngreso) from KOpinion a where statusOpinion in (1,2)";
            }
            else
            {
                query = "select new KOpinion(a.idOpinion, a.descripcion,a.fechaIngreso) from KOpinion a where statusOpinion in (1,2) and CTipoOpinion.idTipo = " + tipoOpinion;
            }
            ArrayList<KOpinion> opiniones = (ArrayList<KOpinion>) sesion.createQuery(query).list();
            transaccion.commit();
            sesion.close();
            return opiniones;
        }
        catch(Exception e)
        {
            return null;
        }
    }
    
    private Object obtenerObjeto (String consulta)
    {
       try
        {
            sesion = HibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            Object objetoRetorno = (Object) sesion.createQuery(consulta);
            sesion.close();
            return objetoRetorno;
        }
        catch(Exception e)
        {
            return null;
        } 
    }
    
    @Override
    public boolean turnar(KOpinion opinionAct)
    {
        try
        {
            boolean exito = false;
            exito = modificar(opinionAct);
            exito = guardarCopias(opinionAct.getKCopias());
            return exito;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return false;
        }
    }

    @Override
    public boolean guardarCopias(Set<KCopia> KCopias) {
        boolean exito = false;
        try
        {
            sesion = HibernateUtil.getSessionFactory().openSession();
            for(KCopia copiaAct:KCopias)
            {
                transaccion = sesion.beginTransaction();
                sesion.save(copiaAct);
                transaccion.commit();
            }
            sesion.close();
            exito = true;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            exito = false;
        }
        
        return exito;
    }

    @Override
    public boolean finalizar(int[] opiniones)
    {
        boolean exito = false;
        try
        {
            sesion = HibernateUtil.getSessionFactory().openSession();
            KOpinion opinionAct = new KOpinion();
            transaccion = sesion.beginTransaction();
            for(int idOpinion:opiniones)
            {
               
                opinionAct = (KOpinion)sesion.load(KOpinion.class,idOpinion);
                opinionAct.setStatusOpinion(6);
                sesion.update(opinionAct);
                
            }
            transaccion.commit();
            sesion.close();
            exito = true;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            exito = false;
        }
        
        return exito;
    }

    @Override
    public ArrayList<Clasificacion> consultarClasificacion(int idArea) {
        ArrayList <Clasificacion> clasificaciones = new ArrayList<>();
        try
        {
            sesion = HibernateUtil.getSessionFactory().openSession();
            transaccion = sesion.beginTransaction();
            for(int i=1; i<=4; i++)
            {
                Clasificacion clasificacionAct = new Clasificacion();
                clasificacionAct.setNoLeidas((int)(long)sesion.createQuery("select count (id_opinion) from KOpinion a where clasificacion = " + i + " and statusOpinion = 3 and CConcepto.CArea.idArea = " + idArea).uniqueResult());
                clasificacionAct.setLeidas((int)(long)sesion.createQuery("select count (id_opinion) from KOpinion a where clasificacion = " + i + " and statusOpinion = 4 and CConcepto.CArea.idArea = " + idArea).uniqueResult());
                clasificacionAct.calculoTotales();
                clasificaciones.add(clasificacionAct);
            }
            transaccion.commit();
            sesion.close();
            return clasificaciones;
        }
        catch(Exception e)
        {
            if(transaccion != null)
            {
                transaccion.rollback();
            }
            return null;
        }
    }

    
    
}
