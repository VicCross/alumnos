/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready
(
    function()
    {
        $('#cbArea').change(function(event)
        {
            //alert("cambio");
            //$('#cbActividades').html("<option value='0'>Selecciona una actividad </option>");
            var area  = $('#cbArea').val();
            if(area != 0)
            {
                $.getJSON('mostrarConcepto', {idArea : area},function(jsonResponse)
                {
                    var resultado = "<option value='0'>Selecciona una actividad</option>";
                    var n = 0;
                    $.each(jsonResponse.conceptosExt, function(i,item) 
                    {
                        n++;
                        //alert("entro a la lista");
                        $.each(jsonResponse.conceptosExt[i], function(j,item) 
                        {
                            if(jsonResponse.conceptosExt[i][j].nivel == 1)
                            {
                                resultado += "<option value='"+jsonResponse.conceptosExt[i][j].idConcepto+"' class='oscure'>"+ jsonResponse.conceptosExt[i][j].nombre +"</option>";
                            }
                            else
                            {
                                var identacion = "";
                                for(var n=1; n<jsonResponse.conceptosExt[i][j].nivel;n++)
                                {
                                    identacion+= "&nbsp;&nbsp;";
                                }
                                resultado += "<option value='"+jsonResponse.conceptosExt[i][j].idConcepto+"'>"+ identacion + jsonResponse.conceptosExt[i][j].nombre +"</option>";
                            }
                            //alert("id: " + jsonResponse.conceptosExt[i][j].idConcepto + "nombre: " + jsonResponse.conceptosExt[i][j].nombre + "Nivel: " + jsonResponse.conceptosExt[i][j].nivel );
                        });
                    });
                    if(n == 0)
                    {
                        resultado = "<option value='0'>No hay actividades</option>";
                    }
                    $('#cbActividades').html(resultado);
                });
            }
            else
            {
                $('#cbActividades').html("<option value='0'>Selecciona una área</option>");
            }
        });
        
        $('#cbProyecto').change(function(event)
        {
            //alert("cambio");
            //$('#cbActividades').html("<option value='0'>Selecciona una actividad </option>");
            var proyecto  = $('#cbProyecto').val();
            if(proyecto != 0)
            {
                $.getJSON('mostrarConceptoProy', {idProyecto : proyecto},function(jsonResponse)
                {
                    var resultado = "<option value='0'>Selecciona una meta</option>";
                    var n = 0;
                    $.each(jsonResponse.conceptosExt, function(i,item) 
                    {
                        n++;
                        //alert("entro a la lista");
                        $.each(jsonResponse.conceptosExt[i], function(j,item) 
                        {
                            if(jsonResponse.conceptosExt[i][j].nivel == 1)
                            {
                                resultado += "<option value='"+jsonResponse.conceptosExt[i][j].idConcepto+"' class='oscure'>"+ jsonResponse.conceptosExt[i][j].nombre +"</option>";
                            }
                            else
                            {
                                var identacion = "";
                                for(var n=1; n<jsonResponse.conceptosExt[i][j].nivel;n++)
                                {
                                    identacion+= "&nbsp;&nbsp;";
                                }
                                resultado += "<option value='"+jsonResponse.conceptosExt[i][j].idConcepto+"'>"+ identacion + jsonResponse.conceptosExt[i][j].nombre +"</option>";
                            }
                            //alert("id: " + jsonResponse.conceptosExt[i][j].idConcepto + "nombre: " + jsonResponse.conceptosExt[i][j].nombre + "Nivel: " + jsonResponse.conceptosExt[i][j].nivel );
                        });
                    });
                    if(n == 0)
                    {
                        resultado = "<option value='0'>No hay metas</option>";
                    }
                    $('#cbMetas').html(resultado);
                });
            }
            else
            {
                $('#cbMetas').html("<option value='0'>Selecciona un proyecto</option>");
            }
        });
        
        $('#radioArea').click(function(){
            if($('#radioArea').is(':checked'))
            {
                $('#contArea').show('slow');
                $('#contProyectos').hide('slow');
                $('#cbMetas').val(0);
                $('#cbActividades').val(0);
                $('#btnDeriv').addClass('disabled');
            }
        });
        
        $('#radioProyecto').click(function(){
            if($('#radioProyecto').is(':checked'))
            {
                $('#contProyectos').show('slow');
                $('#contArea').hide('slow');
                $('#cbMetas').val(0);
                $('#cbActividades').val(0);
                $('#btnDeriv').addClass('disabled');
            }
        });
        
         $('#btnFelicitacion').bind('click',function(){
            //alert('shite');
            $('#btnFelicitacion').css('background-color','green');
            $('#btnPeticion').css('background-color','#808080');
            $('#btnSugerencia').css('background-color','#808080');
            $('#btnQueja').css('background-color','#808080');
            $('#lblDescripcionTipo').text("Felicitaci�n");
            $('#idTipo').val(1);
        });
        
        $('#btnPeticion').bind('click',function(){
            $('#btnFelicitacion').css('background-color','#808080');
            $('#btnPeticion').css('background-color','#FCC545');
            $('#btnSugerencia').css('background-color','#808080');
            $('#btnQueja').css('background-color','#808080');
            $('#lblDescripcionTipo').text("Petici�n");
            $('#idTipo').val(2);
        });
        
        $('#btnSugerencia').bind('click',function(){
            $('#btnFelicitacion').css('background-color','#808080');
            $('#btnPeticion').css('background-color','#808080');
            $('#btnSugerencia').css('background-color','#F6FF81');
            $('#btnQueja').css('background-color','#808080');
            $('#lblDescripcionTipo').text("Sugerencia");
            $('#idTipo').val(3);
        });
        
        $('#btnQueja').bind('click',function(){
            $('#btnFelicitacion').css('background-color','#808080');
            $('#btnPeticion').css('background-color','#808080');
            $('#btnSugerencia').css('background-color','#808080');
            $('#btnQueja').css('background-color','#F83D3D');
            $('#lblDescripcionTipo').text("Queja");
            $('#idTipo').val(4);
        });
        
        $('#cbActividades').bind('change',function(){
            var valor = $('#cbActividades').val();
            if($('#cbActividades').val() != 0)
            {
                $('#btnDeriv').removeClass('disabled');
            }
        });
        
        $('#cbMetas').bind('change',function(){
            if($('#cbMetas').val() != 0)
            {
                $('#btnDeriv').removeClass('disabled');
            }
        });
        
        $('#frmDeriv').submit( function(event){
            if ($('#cbActividades').val() != 0)
            {
                return;
            }
            else if ($('#cbMetas').val() != 0)
            {
                return;
            }
            else
            {
                event.preventDefault();
            }
        });
    }
);
    


