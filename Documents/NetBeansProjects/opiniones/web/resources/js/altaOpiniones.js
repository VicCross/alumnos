
$(document).ready
(
    function()
    {
        $('#cbPais').change(function(event)
        {
            $('#cbDelegacion').html("<option value='0'>Selecci�n</option>");
            var pais = $('#cbPais').val();
            $.getJSON('ajaxAction', {idPais : pais},function(jsonResponse)
            {
                var resultado = "<option value='0'>Selecci�n</option>";
                $.each(jsonResponse.estados, function(i,item) 
                {
                    resultado += "<option value=" + jsonResponse.estados[i].id + ">"+jsonResponse.estados[i].estadonombre + "</option>";
                });
                //alert(resultado);
                if (typeof(resultado) != 'undefined')
                {
                    $('#cbEntidadFederativa').html(resultado);
                }
                else
                {
                    $('#cbEntidadFederativa').html("<option>Sin estados</option>");
                }
            });
        });

        $('#cbEntidadFederativa').change(function(event)
        {
            var estado = $('#cbEntidadFederativa').val();
            $.getJSON('delegacionesAction', {estado : estado},function(jsonResponse)
            {
                var resultado = "<option value='0'>Selección</option>";
                $.each(jsonResponse.delegaciones, function(i,item) 
                {
                    resultado += "<option value=" + jsonResponse.delegaciones[i].idMunicipio + ">"+jsonResponse.delegaciones[i].nombreMunicipio + "</option>";
                });
                //alert(resultado);
                if (typeof(resultado) != 'undefined')
                {
                    $('#cbDelegacion').html(resultado);
                }
                else
                {
                    $('#cbDelegacion').html("<option>Sin estados</option>");
                }
            });
        });
        
        $('#btnFelicitacion').css('background-color','green');
        
        $('#btnFelicitacion').bind('click',function(){
            alert('shite');
            $('#btnFelicitacion').css('background-color','green');
            $('#btnPeticion').css('background-color','#808080');
            $('#btnSugerencia').css('background-color','#808080');
            $('#btnQueja').css('background-color','#808080');
            $('#lblDescripcionTipo').text("Felicitaci�n");
            $('#idTipo').val(1);
        });
        
        $('#btnPeticion').bind('click',function(){
            $('#btnFelicitacion').css('background-color','#808080');
            $('#btnPeticion').css('background-color','#FCC545');
            $('#btnSugerencia').css('background-color','#808080');
            $('#btnQueja').css('background-color','#808080');
            $('#lblDescripcionTipo').text("Petici�n");
            $('#idTipo').val(2);
        });
        
        $('#btnSugerencia').bind('click',function(){
            $('#btnFelicitacion').css('background-color','#808080');
            $('#btnPeticion').css('background-color','#808080');
            $('#btnSugerencia').css('background-color','#F6FF81');
            $('#btnQueja').css('background-color','#808080');
            $('#lblDescripcionTipo').text("Sugerencia");
            $('#idTipo').val(3);
        });
        
        $('#btnQueja').bind('click',function(){
            $('#btnFelicitacion').css('background-color','#808080');
            $('#btnPeticion').css('background-color','#808080');
            $('#btnSugerencia').css('background-color','#808080');
            $('#btnQueja').css('background-color','#F83D3D');
            $('#lblDescripcionTipo').text("Queja");
            $('#idTipo').val(4);
        });
        
        $('#cbTipoOpinion').bind('change',function(){
            if($('#cbTipoOpinion').val() == 4 || $('#cbTipoOpinion').val() == 5)
            {
                $('#contUrlRS').show();
            }
            else
            {
                $('#contUrlRS').hide();
            }
            
        });
        
        $('#btnLimpieza').bind('click',function(){
            $('#frmOpinion').trigger('reset');
            $('#frmOpinion').validator('update');
        });
        
        $('#frmOpinion').validator().on('submit', function (e) {
            if (e.isDefaultPrevented())
            {
                $('#txtModalHeader').text("Revisa los datos");
                $('#iconoExito').hide();
                $('#iconoError').show();
                $('#txtModalBody').text("Intentalo nuevamente");
                $('#modalResultado').modal('show'); 
            }
            else 
            {
                e.preventDefault();
                $('#txtPais').val($('#cbPais option:selected').text());
                if($('#cbEntidadFederativa').val() !== "0")
                {
                    $('#txtEntidadFederativa').val($('#cbEntidadFederativa option:selected').text());
                }
                else
                {
                    $('#txtEntidadFederativa').val('');
                }
                
                if($('#cbDelegacion').val() !== "0")
                {
                    $('#txtDelegacion').val($('#cbDelegacion option:selected').text());
                }
                else
                {
                    $('#txtDelegacion').val('');
                }
                var datos = $('#frmOpinion').serialize();
                    $.ajax({
                        type: "POST",
                        url: "agregarOpinion",
                        data: datos,
                        success: function(data){
                            //var datosJSON = JSON.stringify(data);
                            $('#frmOpinion').trigger('reset');
                            $('#frmOpinion').validator('update');
                            if(data == "correcto")
                            {
                                $('#txtModalHeader').text("Operación exitosa!");
                                $('#iconoExito').show();
                                $('#iconoError').hide();
                                $('#txtModalBody').text("Opinion almacenada");
                                $('#modalResultado').modal('show'); 
                            }
                            else
                            {
                                $('#txtModalHeader').text("Revisa los datos");
                                $('#iconoExito').hide();
                                $('#iconoError').show();
                                $('#txtModalBody').text("Opinion no almacenada");
                                $('#modalResultado').modal('show'); 
                            }
                        },
                        error: function(){
                            alert("conexión fallida");
                        }
                    });
            }
        });
    }
);


