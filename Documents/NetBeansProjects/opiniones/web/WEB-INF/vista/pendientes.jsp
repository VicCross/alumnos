<%-- 
    Document   : pendientes
    Created on : 26/09/2016, 01:00:25 PM
    Author     : VicCross
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
        <title>Cry, cry, cry babyyyyyyy, Janis joplin, where the hell are you ?</title>
    </head>
    <body>
        <!--<h1>Honey, this life is just about shite, fuck this !!</h1>-->
        <h2><s:property value="clasif"></s:property> pendientes (<s:property value="area"></s:property>)</h2>
        <table class="table">
            <s:iterator value="opiniones">
                <tr>
                    <td width="40%"><s:property value="nombreConcepto"></s:property></td>
                    <td width="60%">
                        <s:iterator value="opiniones">
                            <a href="verOpinionDeriv?idOpinion=<s:property value="idOpinion"></s:property>"><p><s:property value="descripcion"></s:property> <s:property value="fecha"></s:property></p></a>
                        </s:iterator>
                    </td>
                </tr>
            </s:iterator>
        </table>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.0.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    </body>
</html>
