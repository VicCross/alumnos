<%@page import="model.entities.CPais"%>
<%@page import="controller.PaisController"%>
<%@page import="model.entities.CTipoOpinion"%>
<%@page import="controller.TipoController"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.entities.CExposicion"%>
<%@page import="controller.ExposicionController"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="ISO-8859-1">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Opiniones</title>
	<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/estilos.css">
</head>
<body>
	<div class="container">
            
                <div class="modal fade" id="modalResultado" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <div id="txtModalHeader" class="modal-title">titulo del modal</div>
                            </div>
                            <div class="modal-body">
                                <span id="iconoExito" class="glyphicon glyphicon-saved"></span>
                                <span id="iconoError" class="glyphicon glyphicon-remove"></span>
                                <p id="txtModalBody">Resultado de la operaci�n</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
            
		<div id="contRuta">
			<p>Aplicaciones > Opiniones > Redactar opini�n</p>
		</div>
		<div id="contTitulo">
			<p>Redactar opini�n</p>
		</div>
            <form id="frmOpinion" class="form-inline" role="form">
			<div class="row">
				<div class="col-xs-12 col-sm-2 col-md-2 hidden-xs">
					<button type="button" class="btn" id="btnRegresar">Regresar</button>
				</div>
				<div class="form-group col-xs-12 col-sm-10 col-md-10" id="fldExpo">
					<label class="col-md-2">Exposici�n:</label>
					<select id="cbEcposicion" name="cbEcposicion">
                                            <%
                                                ExposicionController ExposicionAct = new ExposicionController();
                                                ExposicionAct.mostrar();
                                                ArrayList <CExposicion> exposiciones = ExposicionAct.getExposiciones();
                                                for (CExposicion expoBucle: exposiciones )
                                                {
                                                   out.println("<option value="+expoBucle.getIdExposicion()+">"+expoBucle.getNombre()+"</option>");
                                                }
                                            %>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-xs-12 col-sm-10 col-md-10 col-sm-offset-2 col-md-offset-2" id="fldTipoOpinion">
					<label class="col-md-2">Opini�n:</label>
					<select id="cbTipoOpinion" name="cbTipoOpinion">
                                            <%
                                                TipoController tipoAct = new TipoController();
                                                tipoAct.listar();
                                                ArrayList <CTipoOpinion> tipos = tipoAct.getListaTipos();
                                                for (CTipoOpinion opinionBucle: tipos )
                                                {
                                                   out.println("<option value="+opinionBucle.getIdTipo()+">"+opinionBucle.getNombre()+"</option>");
                                                }
                                            %>  
					</select>
				</div>
			</div>
			<div id="contClasificacion" class="col-xs-offset-2">
				<div class="row" >
					<div class="col-xs-12 col-md-3">
						<button type="button" class="btn-default" id="btnFelicitacion">Felicitaci�n</button>
					</div>
					<div class="col-xs-12 col-md-3">
						<button type="button" class="btn-default" id="btnPeticion">Petici�n</button>
					</div>
					<div class="col-xs-12 col-md-3">
						<button type="button" class="btn-default" id="btnSugerencia">Sugerencia</button>
					</div>
					<div class="col-xs-12 col-md-3">
						<button type="button" class="btn-default" id="btnQueja">Queja</button>
					</div>
				</div>
			</div>
			<div id="contSup">
				<div class="form-group">
					<label id="lblFolio">Folio:</label>
					<input type="text" id="txtFolio" name="txtFolio">
				</div>
				<div class="form-group">
					<label id="lblFecha">Fecha:</label>
					<input type="date" id="txtFecha" name="txtFecha" required>
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-2 tituloContenedor">Datos personales</div>
			</div>
			<div id="contDatosPersonales" class="contenedoresDatos col-md-offset-2">
				<div class="row">
                                    
                                    <div class="col-md-6">
					<div class="form-group has-feedback">
						<label id="lblNombre" for="txtNombre" class="control-label">Nombre (s)*:</label>
                                                <input type="text" name="txtNombre" id="txtNombre" class="form-control" pattern="^[a-zA-Z������������0]{1,}[a-zA-Z������������ ]{1,}$" data-error="Revisa este campo" maxlength="20" required>
                                                <div class="help-block with-errors"></div>
					</div>
                                    </div>
                                    <div class="col-md-6">
					<div class="form-group has-feedback">
						<label id="lblEdad" for="txtEdad"  class="control-label">Edad:</label>
						<input type="number" name="txtEdad" type="number" min="1" max="100" step="1" id="txtEdad" class="form-control">
                                                <div class="help-block with-errors"></div>
					</div>
                                    </div>
				</div>
				<div class="row">
                                    <div class="col-md-6">
					<div class="form-group has-feedback">
						<label id="lblApellidoPat" class="control-label" for="txtApellidoPat">Apellido paterno*:</label>
						<input type="text" name="txtApellidoPat" id="txtApellidoPat" class="form-control" pattern="^[a-zA-Z������������]{1,}[a-zA-Z������������ ]{1,}$" data-error="Revisa este campo" maxlength="20">
                                                <div class="help-block with-errors"></div>
					</div>
                                    </div>
                                    <div class="col-md-6">
					<div class="form-group has-feedback">
						<label id="lblEmail" class="control-label" for="txtEmail">Correo electronico*:</label>
						<input type="email" name="txtEmail" id="txtEmail" class="form-control" data-error="Revisa el correo ;)" maxlength="30">
                                                <div class="help-block with-errors"></div>
					</div>
                                    </div>
				</div>
				<div class="row">
                                    <div class="col-md-6">
					<div class="form-group has-feedback">
						<label id="lblApellidoMat" class="control-label" for="txtApellidoMat">Apellido materno:</label>
						<input type="text" name="txtApellidoMat"
						id="txtApellidoMat" name="txtApellidoMat" class="form-control" pattern="^[a-zA-Z������������0-9]{1,}[a-zA-Z����� ]{1,}$" data-error="Revisa este campo" maxlength="20">
                                                <div class="help-block with-errors"></div>
					</div>
                                    </div>
                                    <div class="col-md-6">
					<div class="form-group has-feedback">
						<label id="lblTelefono" class="control-label" class="control-label" for="txtTelefono">Telefono:</label>
						<input type="text" name="txtTelefono" id="txtTelefono" class="form-control" pattern="^[0-9]{1,}$" data-error="Revisa este campo" maxlength="15">
                                                <div class="help-block with-errors"></div>
					</div>
                                    </div>
				</div>
                                <div class="row" id="contUrlRS">
                                    <div class="col-md-6">
					<div class="form-group has-feedback">
						<label id="lblUrl" for="txtUrl" class="control-label">URL red social:</label>
						<input type="url" name="txtUrl" id="txtUrl" class="form-control"  data-error="Revisa este campo" maxlength="50">
					</div>
                                    </div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-offset-2 tituloContenedor">Domicilio</div>
			</div>
			<div id="contDomicilio" class="contenedoresDatos col-md-offset-2">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                            <label id="lblPais" for="cbPais" class="control-label">Pa�s:</label>
                                            <select name="cbPais" id="cbPais" class="form-control" data-error="Revisa este campo">
                                                <%
                                                    PaisController paisAct = new PaisController();
                                                    paisAct.listar();
                                                    ArrayList <CPais> paises = paisAct.getPaises();
                                                    for(CPais pais:paises)
                                                    {
                                                       out.println("<option value="+pais.getId()+">"+pais.getPaisnombre()+"</option>");
                                                    }
                                                %>
                                            </select>
                                            <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                            <label id="lblEntidadFederativa" for="cbEntidadFederativa" class="control-label">Entidad Federativa:</label>
                                            <select name="cbEntidadFederativa" id="cbEntidadFederativa" class="form-control">
                                                <option value="0">Selecciona un estado</option>
                                            </select>
                                            <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                            <label id="lblDelegacion" for="cbDelegacion" class="control-label">Delegaci�n o Municipio:</label>
                                            <select name="cbDelegacion" id="cbDelegacion" class="form-control">
                                                    <option value="0">Selecci�n</option>
                                                    <option>Coyoacan</option>
                                                    <option>WTH</option>
                                            </select>
                                            <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                            <label id="lblColonia" for="txtColonia" class="control-label">Colonia:</label>
                                            <input type="text" name="txtColonia" id="txtColonia" class="form-control" pattern="^[a-zA-Z������������0-9]{1,}[a-zA-Z������������0-9 #.-]{1,}$" data-error="Revisa este campo" maxlength="50">
                                            <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                            <label id="lblCalle" for="txtCalle" class="control-label">Calle:</label>
                                            <input type="text" name="txtCalle" id="txtCalle" class="form-control" pattern="^[a-zA-Z������������0-9]{1,}[a-zA-Z������������0-9 #.-]{1,}$" data-error="Revisa este campo" maxlength="50">
                                            <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                            <label  id="lblNumExt" for="txtNumExt" class="control-label">N� Exterior:</label>
                                            <input type="text" name="txtNumExt" id="txtNumExt" class="form-control" pattern="^[a-zA-Z0-9]{1,}[a-zA-Z������������0-9 #.-]{1,}$" data-error="Revisa este campo" maxlength="20">
                                            <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                            <label id="lblCp" for="txtCp" class="control-label">C.P:</label>
                                            <input type="text" name="txtCp" id="txtCp" class="form-control" pattern="^[0-9]{1,}$" data-error="Revisa este campo" maxlength="10">
                                            <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group has-feedback">
                                            <label id="lblNumInt" for="txtNumInt" class="control-label">N� Interior:</label>
                                            <input type="text" name="txtNumInt" id="txtNumInt" class="form-control" pattern="^[a-zA-Z��0-9]{1,}[a-zA-Z������������0-9 #.-]{1,}$" data-error="Revisa este campo" maxlength="20">
                                            <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
			</div>
			<div class="row">
				<div class="col-md-offset-2 tituloContenedor">Datos del servidor publico denunciado</div>
			</div>
			<div id="contServidor" class="contenedoresDatos col-md-offset-2">
				<div class="row">
                                    <div class="col-md-12">
					<div class="form-group has-feedback">
						<label id="lblNombreServidor" for="txtNombreServidor" class="control-label">Nombre:</label>
						<input type="text" id="txtNombreServidor" name="txtNombreServidor" class="form-control" pattern="^[a-zA-Z]{1,}[a-zA-Z����� ]{1,}$" data-error="Revisa este campo" maxlength="50">
                                                <div class="help-block with-errors"></div>
					</div>
                                    </div>
				</div>
				<div class="row">
                                    <div class="col-md-6">
					<div class="form-group has-feedback">
						<label id="lblCargo" for="txtCargo" class="control-label">Cargo:</label>
						<input type="text" name="txtCargo" id="txtCargo" class="form-control" pattern="^[a-zA-Z]{1,}[0-9a-zA-Z������������ ]{1,}$" data-error="Revisa este campo" maxlength="50">
                                                <div class="help-block with-errors"></div>
					</div>
                                    </div>
                                    <div class="col-md-6">
					<div class="form-group has-feedback">
						<label id="lblNumLugarAds" for="txtNumLugarAds" class="control-label">Lugar de adscripcion:</label>
						<input type="text" name="txtNumLugarAds" id="txtNumLugarAds" class="form-control" pattern="^[a-zA-Z]{1,}[0-9a-zA-Z������������ ]{1,}$" data-error="Revisa este campo" maxlength="50">
                                                <div class="help-block with-errors"></div>
					</div>
                                    </div>
				</div>
			</div>

			<div class="row">
				<div id="lblDescripcionTipo" class="col-md-offset-2 tituloContenedor">Felicitaci�n</div>
			</div>
			<div id="contDescripcion" class="contenedoresDatos col-md-offset-2">
				<div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
					<div class="form-group has-feedback">
						<label id="lblDescripcion" for="txtDescripcion" class="control-label">Descripci�n*</label>
						<textarea id="txtDescripcion" name="txtDescripcion" rows="9" cols="120" class="form-control" pattern="^[0-9a-zA-Z������������ ]{1,}$" data-error="Revisa este campo" maxlength="1000" required></textarea>
                                                <div class="help-block with-errors"></div>
					</div>
                                    </div>
				</div>
                                <br>
				<div class="row">
                                    
					<div class="form-group col-md-12" id="contButtons">
						<button type="submit" class="btn-default">Enviar</button>
						<button type="button" id="btnLimpieza" name="btnLimpieza" class="btn-default">Borrar</button>
					</div>
				</div>
			</div>
                        <input id="idTipo" value="1" name="idTipo" type="hidden" >
                        <input id="txtPais" name="txtPais" type="hidden" >
                        <input id="txtEntidadFederativa" name="txtEntidadFederativa" type="hidden" >
                        <input id="txtDelegacion" name="txtDelegacion" type="hidden" >
		</form>
	</div>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.0.js"></script> <!-- agregar a tabla de base de datos -->
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/validator.min.js" ></script>
    <script src="${pageContext.request.contextPath}/resources/js/altaOpiniones.js" charset="ISO-8859-1"></script>
    
    
</body>
</html>