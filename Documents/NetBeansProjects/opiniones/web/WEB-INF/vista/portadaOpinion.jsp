<%-- 
    Document   : portadaOpinion
    Created on : 23/09/2016, 11:35:56 AM
    Author     : VicCross
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/derivOpinion.css">
        <title>Cage the elephant</title>
    </head>
    <body>
        <div class="container">
            <h1>La misteriosa portada de <s:property value="area"></s:property></h1>
            <div id="contContadores" name="contContadores">
                <p>Opiniones pendientes</p>
                <div id="contClasicicaciones" name="contClasicicaciones">
                    <div id="contFelicitaciones">
                        <div class="row">
                            <div class="col-xs-2 col-md-2">Totales</div>
                            <div class="col-xs-2 col-md-2">Leídas</div>
                            <div class="col-xs-2 col-md-2">Sin leer</div>
                        </div>
                        <div class="row">
                            <s:iterator value="clasificaciones" begin="0" end="0">
                                 <div class="col-xs-2 col-md-2"><s:property value='totales'></s:property></div>
                                 <div class="col-xs-2 col-md-2"><s:property value='leidas'></s:property></div>
                                 <div class="col-xs-2 col-md-2"><s:property value='noLeidas'></s:property></div>
                            </s:iterator>
                            <a href="mostrarPendientes?idArea=<s:property value="idArea"></s:property>&clasificacion=1&area=<s:property value="area"></s:property>"> <div class="col-xs-6 col-md-6">Felicitaciones</div> </a>
                        </div>
                    </div>
                    <div id="contPeticiones">
                        <div class="row">
                            <s:iterator value="clasificaciones" begin="1" end="1">
                                 <div class="col-xs-2 col-md-2"><s:property value='totales'></s:property></div>
                                 <div class="col-xs-2 col-md-2"><s:property value='leidas'></s:property></div>
                                 <div class="col-xs-2 col-md-2"><s:property value='noLeidas'></s:property></div>
                            </s:iterator>
                            <a href="mostrarPendientes?idArea=<s:property value="idArea"></s:property>&clasificacion=2&area=<s:property value="area"></s:property>"> <div class="col-xs-6 col-md-6">Peticiones</div> </a>
                        </div>
                    </div>
                    <div id="contSugerencias">
                        <div class="row">
                            <s:iterator value="clasificaciones" begin="2" end="2">
                                 <div class="col-xs-2 col-md-2"><s:property value='totales'></s:property></div>
                                 <div class="col-xs-2 col-md-2"><s:property value='leidas'></s:property></div>
                                 <div class="col-xs-2 col-md-2"><s:property value='noLeidas'></s:property></div>
                            </s:iterator>
                            <a href="mostrarPendientes?idArea=<s:property value="idArea"></s:property>&clasificacion=3&area=<s:property value="area"></s:property>"> <div class="col-xs-6 col-md-6">Sugerencias</div> </a>
                        </div>
                    </div>
                    <div id="contQuejas">
                        <div class="row">
                            <s:iterator value="clasificaciones" begin="3" end="3">
                                 <div class="col-xs-2 col-md-2"><s:property value='totales'></s:property></div>
                                 <div class="col-xs-2 col-md-2"><s:property value='leidas'></s:property></div>
                                 <div class="col-xs-2 col-md-2"><s:property value='noLeidas'></s:property></div>
                            </s:iterator>
                            <a href="mostrarPendientes?idArea=<s:property value="idArea"></s:property>&clasificacion=4&area=<s:property value="area"></s:property>"> <div class="col-xs-6 col-md-6">Quejas</div> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.0.js"></script> <!-- agregar a tabla de base de datos -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    </body>
</html>
