<%-- 
    Document   : derivOpinionError
    Created on : 20/09/2016, 03:35:26 PM
    Author     : VicCross
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/derivOpinion.css">
        <title>Estamos trabajando en ello</title>
    </head>
    <body>
        <h1>No se pudo turnar la opinión, verifica tus datos e intentalo nuevamente :(</h1>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.0.js"></script> <!-- agregar a tabla de base de datos -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script>
            setTimeout(function()
            {
                Window.location = "/mostrarCentral";
            }, 5000);
        </script>
    </body>
</html>
