<%-- 
    Document   : derivarOpinion
    Created on : 5/09/2016, 03:11:53 PM
    Author     : VicCross
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/derivOpinion.css">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <div id="contRuta">
                <p>Aplicaciones > Opiniones > Central de opiniones > Derivar una opinión</p>
            </div>
            <div id="contTitulo">
                <p>Derivar opinión</p>
            </div>
            <div>
                <div class="row" >
                    <div class="col-md-3">
                        <div class="col-xs-12 col-sm-2 col-md-12 hidden-xs">
                            <button type="button" class="btn" id="btnRegresar" >Regresar</button>
                        </div>
                    </div>
                    <div class="col-md-9" id="contClasificacion">
                        <div class="col-xs-12 col-md-3">
                            <s:if test="%{opinionAct.clasificacion == 1}">
                                <button type="button" class="btn-default btnClasificacion botonFelicitacion" id="btnFelicitacion">Felicitación</button>
                            </s:if>
                            <s:else>
                                <button type="button" class="btn-default btnClasificacion" id="btnFelicitacion">Felicitación</button>
                            </s:else>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <s:if test="%{opinionAct.clasificacion == 2}">
                                <button type="button" class="btn-default btnClasificacion botonPeticion" id="btnPeticion">Petición</button>
                            </s:if>
                            <s:else>
                                <button type="button" class="btn-default btnClasificacion" id="btnPeticion">Petición</button>
                            </s:else>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <s:if test="%{opinionAct.clasificacion == 3}">
                                <button type="button" class="btn-default btnClasificacion botonSugerencia" id="btnSugerencia">Sugerencia</button>
                            </s:if>
                            <s:else>
                                <button type="button" class="btn-default btnClasificacion" id="btnSugerencia">Sugerencia</button>
                            </s:else>
                        </div>
                        <div class="col-xs-12 col-md-3">
                            <s:if test="%{opinionAct.clasificacion == 4}">
                                <button type="button" class="btn-default btnClasificacion botonqueja" id="btnQueja">Queja</button>
                            </s:if>
                            <s:else>
                                <button type="button" class="btn-default btnClasificacion" id="btnQueja">Queja</button>
                            </s:else>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="contenedoresDatos">
                <div class="row" >
                    <div id="txtDescripcion" name="txtDescripcion" class="col-md-8">
                        <p><b>Descripción:</b> <s:property value="opinionAct.descripcion" /></p>
                    </div>
                    <div id="contTags" name="contTags" class="col-md-4">
                        <div><s:property value="opinionAct.fechaIngreso" /></div>
                        <div><s:property value="opinionAct.CTipoOpinion.nombre" /></div>
                        <div><s:property value="opinionAct.CExposicion.nombre" /></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="tituloContenedor">Datos personales</div>
            </div>
            <div class="contenedoresDatos">
                <div class="row" >
                    <div id="txtNombre" name="txtNombre" class="col-md-3">
                        <p><b>Nombre :</b> <s:property value="opinionAct.KPersonaOpinion.nombre" /> <s:property value="opinionAct.KPersonaOpinion.apellidoPaterno" /> <s:property value="opinionAct.KPersonaOpinion.apellidoMaterno" /></p>
                    </div>
                    <s:if test="%{opinionAct.KPersonaOpinion.email != '' && opinionAct.KPersonaOpinion.email != null}">
                        <div id="txtEmail" name="txtEmail" class="col-md-3">
                            <p><b>Email :</b> <s:property value="opinionAct.KPersonaOpinion.email" /> </p>
                        </div>
                    </s:if>
                    <s:if test="%{opinionAct.KPersonaOpinion.telefono != '' && opinionAct.KPersonaOpinion.telefono != null}">
                        <div id="txtTelefono" name="txtTelefono" class="col-md-3">
                            <p><b>Telefono :</b> <s:property value="opinionAct.KPersonaOpinion.telefono" /> </p>
                        </div>
                    </s:if>
                    <div id="txtEdad" name="txEdad" class="col-md-3">
                        <p><b>Edad :</b> <s:property value="opinionAct.KPersonaOpinion.edad" /> </p>
                    </div>
                    <div id="contTags" name="contTags" class="col-md-4">
                    </div>
                </div>
                <div class="row" >
                    <div id="txtNacionalidad" name="txtNacionalidad" class="col-md-3">
                        <p><b>Pais :</b> <s:property value="opinionAct.KPersonaOpinion.nacionalidad" /> </p>
                    </div>
                    <s:if test="%{opinionAct.KPersonaOpinion.estado != '' && opinionAct.KPersonaOpinion.estado != null}">
                        <div id="txtEstado" name="txtEstado" class="col-md-3">
                            <p><b>Estado :</b> <s:property value="opinionAct.KPersonaOpinion.estado" /> </p>
                        </div>
                    </s:if>
                    <s:if test="%{opinionAct.KPersonaOpinion.delegacion != '' && opinionAct.KPersonaOpinion.delegacion != null}">
                        <div id="txtDelegacion" name="txtDelegacion" class="col-md-3">
                            <p><b>Delegacion o municipio :</b> <s:property value="opinionAct.KPersonaOpinion.delegacion" /> </p>
                        </div>
                    </s:if>
                    <s:if test="%{opinionAct.KPersonaOpinion.colonia != '' && opinionAct.KPersonaOpinion.colonia != null}">
                        <div id="txtDelegacion" name="txtColonia" class="col-md-3">
                            <p><b>Colonia :</b> <s:property value="opinionAct.KPersonaOpinion.colonia" /> </p>
                        </div>
                    </s:if>
                </div>
                <div class="row" >
                    <s:if test="%{opinionAct.KPersonaOpinion.calle != '' && opinionAct.KPersonaOpinion.calle != null}">
                        <div id="txtCalle" name="txtCalle" class="col-md-3">
                            <p><b>Calle :</b> <s:property value="opinionAct.KPersonaOpinion.calle" /> </p>
                        </div>
                    </s:if>
                    <s:if test="%{opinionAct.KPersonaOpinion.noExt != '' && opinionAct.KPersonaOpinion.noExt != null}">
                        <div id="txtNoExt" name="txtNoExt" class="col-md-3">
                            <p><b>Numero exterior :</b> <s:property value="opinionAct.KPersonaOpinion.noExt" /> </p>
                        </div>
                    </s:if>
                    <s:if test="%{opinionAct.KPersonaOpinion.noInt != '' && opinionAct.KPersonaOpinion.noInt != null}">
                        <div id="txtNoInt," name="txtNoInt" class="col-md-3">
                            <p><b>Numero interior :</b> <s:property value="opinionAct.KPersonaOpinion.noInt" /> </p>
                        </div>
                    </s:if>
                    <s:if test="%{opinionAct.KPersonaOpinion.codigoPostal != '' && opinionAct.KPersonaOpinion.codigoPostal != null}">
                        <div id="txtCP" name="txtCP" class="col-md-3">
                            <p><b>C.P. :</b> <s:property value="opinionAct.KPersonaOpinion.codigoPostal" /> </p>
                        </div>
                    </s:if>
                </div>
            </div>
            <s:if test="%{opinionAct.KServidorDenunciado.nombre != '' && opinionAct.KServidorDenunciado.nombre != null}">
                <div class="row">
                    <div class="tituloContenedor">Servidor denunciado</div>
                </div>
                <div class="contenedoresDatos">
                    <div class="row" >
                        <div id="txtNombreServidor" name="txtNombreServidor" class="col-md-3">
                            <p><b>Nombre :</b> <s:property value="opinionAct.KServidorDenunciado.nombre" /> </p>
                        </div>
                        <s:if test="%{opinionAct.KServidorDenunciado.cargo != '' && opinionAct.KServidorDenunciado.cargo != null}">
                            <div id="txtCargo" name="txtCargo" class="col-md-3">
                                <p><b>Cargo :</b> <s:property value="opinionAct.KServidorDenunciado.cargo" /> </p>
                            </div>
                        </s:if>
                        <s:if test="%{opinionAct.KServidorDenunciado.lugar != '' && opinionAct.KServidorDenunciado.lugar != null}">
                            <div id="txtLugar" name="txtLugar" class="col-md-3">
                                <p><b>Lugar de adscripción:</b> <s:property value="opinionAct.KServidorDenunciado.lugar" /> </p>
                            </div>
                        </s:if>
                    </div>
                </div>
            </s:if><!-- fuck -->
            <form id="frmDeriv" action="derivar" method="post" onsubmit="return validarArea">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="contSubTitulo" class="contSubTitulo">
                                <p>Selecciona un destino</p>
                            </div>
                            <div id="contDeriv">
                                <div class="contenedoresDatos">
                                    <div class="radio">
                                        <label><input type="radio" name="radioDestino" id="radioArea">Área</label>
                                    </div>
                                </div>
                                <div id="contArea" class="contenedoresAreasProys">
                                    <select id="cbArea" name="cbArea">
                                        <option value="0">Selecciona una área</option>
                                        <s:iterator value="areas">
                                            <option value="<s:property value='idArea'></s:property>"><s:property value="nombre"></s:property></option>
                                        </s:iterator>
                                    </select>
                                    <select id="cbActividades" name="cbActividades">
                                        <option value="0">Selecciona una área</option>
                                    </select>
                                </div>
                                <div class="contenedoresDatos">
                                    <div class="radio">
                                        <label><input type="radio" name="radioDestino" id="radioProyecto">Proyecto</label>
                                    </div>
                                </div>
                                <div id="contProyectos" class="contenedoresAreasProys">
                                    <select id="cbProyecto" name="cbProyecto">
                                        <option value="0">Selecciona un proyecto</option>
                                        <s:iterator value="proyectos">
                                            <option value="<s:property value='idProyecto'></s:property>"><s:property value="nombre"></s:property></option>
                                        </s:iterator>
                                    </select>
                                    <select id="cbMetas" name="cbMetas">
                                        <option value="0">Selecciona un proyecto</option>
                                    </select>
                                </div>
                            </div>
                            <div>
                                <div></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div id="contSubTitulo1" class="contSubTitulo col-md-12">
                                    <p>Envia algunas copias</p>
                                </div>
                            </div>
                            <div class="contenedoresDatos">
                                <div>Área</div>
                            </div>
                            <!--
                            <div class="row">
                                <div class="col-md-12">
                                    hola
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="checkbox-inline"><input type="checkbox" value="">Option 1</label>
                                </div>
                                <div class="col-md-3">
                                    <label class="checkbox-inline"><input type="checkbox" value="">Option 1</label>
                                </div>
                                <div class="col-md-3">
                                    <label class="checkbox-inline"><input type="checkbox" value="">Option 1</label>
                                </div>
                                <div class="col-md-3">
                                    <label class="checkbox-inline"><input type="checkbox" value="">Option 1</label>
                                </div>
                                <div class="col-md-3">
                                    <label class="checkbox-inline"><input type="checkbox" value="">Option 1</label>
                                </div>
                            </div>
                            -->
                            <div class="row contenedoresAreasProys">
                                <s:iterator value="areas">
                                    <div class="col-md-3">
                                        <label class="checkbox-inline"><input type="checkbox" name="copiasArea" value="<s:property value='idArea'></s:property>"><s:property value="nombre"></s:property></label>
                                    </div>
                                </s:iterator>
                            </div>
                            <div class="contenedoresDatos">
                                <div>Proyectos</div>
                            </div>
                            <div class="row contenedoresAreasProys">
                                <s:iterator value="proyectos">
                                    <div class="col-md-6">
                                        <label class="checkbox-inline"><input type="checkbox" name="copiasProyecto" value="<s:property value='idProyecto'></s:property>"><s:property value="nombre"></s:property></label>
                                    </div>
                                </s:iterator>
                            </div>
                        </div>
                        <div id="contBtnDerivar" name="contBtnDerivar">
                            <button id="btnDeriv" name="btnDeriv" class="btn btn-success disabled">Derivar Opinión</button>
                        </div>
                    </div>
                    <input type="hidden" id="idOpinion" name="idOpinion" value="<s:property value='opinionAct.idOpinion'></s:property>">
                    <input type="hidden" id="idTipo" name="idTipo" value="<s:property value='opinionAct.clasificacion'></s:property>">
                </form>
            <!-- <h1>Aqui es donde comienza la derivación de las opiniones</h1>
            <div><s:property value="opinionAct.CTipoOpinion.idTipo" /></div>
            <div><s:property value="opinionAct.CTipoOpinion.nombre" /></div>
            <div><s:property value="opinionAct.clasificacion" /></div>
            <div><s:property value="opinionAct.descripcion" /></div>
            <s:if test="%{idOpinion == 8}">
                <div>Es 8 !!</div>
            </s:if> -->
        </div>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.0.js"></script> <!-- agregar a tabla de base de datos -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/derivOpinion.js"></script>
    </body>
</html>
