<%-- 
    Document   : centralOpiniones
    Created on : 1/09/2016, 04:35:03 PM
    Author     : VicCross
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/estilosCentral.css">
        <title>Central de opiniones</title>
    </head>
    <body>
        <div class="container">
            <div id="contRuta">
                <p>Aplicaciones > Opiniones > Central de opiniones</p>
            </div>
            <div id="contTitulo">
		<p>Derivar opiniones</p>
            </div>
            <div class="btn-group-wrap">
                <div id="contBotones" class="btn-group" role="group">
                    <button type="button" class="btn btn-primary" id="btnEscritas" value="1">Escritas</button>
                    <button type="button" class="btn btn-primary" id="btnWeb" value="2">Web</button>
                    <button type="button" class="btn btn-primary" id="btnKiosko" value="3">Kiosko</button>
                    <button type="button" class="btn btn-primary" id="btnFacebook" value="4">Facebook</button>
                    <button type="button" class="btn btn-primary" id="btnTwitter" value="5">Twitter</button>
                    <button type="button" class="btn btn-primary" id="btnEmail" value="6">Email</button>
                    <button type="button" class="btn btn-primary" id="btnGerencia" value="7">Gerencia</button>
                    <button type="button" class="btn btn-primary" id="btnInba" value="8">INBA</button>
                </div>
            </div>
            <form id="frmCentral" action="finalizar" method="post">
                <div id="contTabla">
                    <div class="panel panel-default">
                        <table class="table table-condensed" >
                            <thead class="thead-inverse">
                                <th> <div class="checkbox"><label><input type="checkbox" id="chkSeleccion" value="">#</label></div> </th>
                                <th> Opiniones </th>
                                <th> Fecha &nbsp;&nbsp;</th>
                            </thead>
                        </table>
                        <div class="div-table-content">
                            <table class="table table-bordered table-condensed" id="contenidoOpiniones">
                                <tbody>
                                    <!--
                                    <tr>
                                        <td><div class="checkbox"><label><input type="checkbox" value=""></label></div></td>
                                        <td><a href="derivarOpinion?idOpinion=1"> Opinion x </a></td>
                                        <td>01/01/1970</td>
                                    </tr>
                                    <tr>
                                        <td><div class="checkbox"><label><input type="checkbox" value=""></label></div></td>
                                        <td>Muy lejos, más allá de las montañas de palabras, alejados de los países de las vocales y las consonantes, viven los textos simulados. Viven aislados en casas de letras, en la costa de la semántica, un gran océano de lenguas. Un riachuelo llamado Pons fluye por su pueblo y los abastece con las normas necesarias. Hablamos de un país paraisomático en el que a uno le caen pedazos de frases asadas en la boca. Ni siquiera los todopoderosos signos de puntuación dominan a los textos simulados; una vida, se puede decir, poco ortográfica. Pero un buen día, una pequeña línea de texto simulado, llamada Lorem Ipsum, decidió aventurarse y salir al vasto mundo de la gramática. El gran Oxmox le desanconsejó hacerlo, ya que esas tierras estaban llenas de comas malvadas, signos de interrogación salvajes y puntos y coma traicioneros, pero el texto simulado no se dejó atemorizar. Empacó sus siete versales, enfundó su inicial en el cinturón y se puso en camino. Cuando ya había escalado las primeras colinas de las montañas cursivas, se dio media vuelta para dirigir su mirada por última vez, hacia su ciudad natal Letralandia, el encabezamiento del pueblo Alfabeto y el subtítulo de su</td>
                                        <td>01/01/1970</td>
                                    </tr>
                                    <tr>
                                        <td><div class="checkbox"><label><input type="checkbox" value=""></label></div></td>
                                        <td>Opinion x</td>
                                        <td>01/01/1970</td>
                                    </tr>
                                    <tr>
                                        <td><div class="checkbox"><label><input type="checkbox" value=""></label></div></td>
                                        <td>Muy lejos, más allá de las montañas de palabras, alejados de los países de las vocales y las consonantes, viven los textos simulados. Viven aislados en casas de letras, en la costa de la semántica, un gran océano de lenguas. Un riachuelo llamado Pons fluye por su pueblo y los abastece con las normas necesarias. Hablamos de un país paraisomático en el que a uno le caen pedazos de frases asadas en la boca. Ni siquiera los todopoderosos signos de puntuación dominan a los textos simulados; una vida, se puede decir, poco ortográfica. Pero un buen día, una pequeña línea de texto simulado, llamada Lorem Ipsum, decidió aventurarse y salir al vasto mundo de la gramática. El gran Oxmox le desanconsejó hacerlo, ya que esas tierras estaban llenas de comas malvadas, signos de interrogación salvajes y puntos y coma traicioneros, pero el texto simulado no se dejó atemorizar. Empacó sus siete versales, enfundó su inicial en el cinturón y se puso en camino. Cuando ya había escalado las primeras colinas de las montañas cursivas, se dio media vuelta para dirigir su mirada por última vez, hacia su ciudad natal Letralandia, el encabezamiento del pueblo Alfabeto y el subtítulo de su</td>
                                        <td>01/01/1970</td>
                                    </tr>
                                    <tr>
                                        <td><div class="checkbox"><label><input type="checkbox" value=""></label></div></td>
                                        <td>Muy lejos, más allá de las montañas de palabras, alejados de los países de las vocales y las consonantes, viven los textos simulados. Viven aislados en casas de letras, en la costa de la semántica, un gran océano de lenguas. Un riachuelo llamado Pons fluye por su pueblo y los abastece con las normas necesarias. Hablamos de un país paraisomático en el que a uno le caen pedazos de frases asadas en la boca. Ni siquiera los todopoderosos signos de puntuación dominan a los textos simulados; una vida, se puede decir, poco ortográfica. Pero un buen día, una pequeña línea de texto simulado, llamada Lorem Ipsum, decidió aventurarse y salir al vasto mundo de la gramática. El gran Oxmox le desanconsejó hacerlo, ya que esas tierras estaban llenas de comas malvadas, signos de interrogación salvajes y puntos y coma traicioneros, pero el texto simulado no se dejó atemorizar. Empacó sus siete versales, enfundó su inicial en el cinturón y se puso en camino. Cuando ya había escalado las primeras colinas de las montañas cursivas, se dio media vuelta para dirigir su mirada por última vez, hacia su ciudad natal Letralandia, el encabezamiento del pueblo Alfabeto y el subtítulo de su</td>
                                        <td>01/01/1970</td>
                                    </tr>
                                    <tr>
                                        <td><div class="checkbox"><label><input type="checkbox" value=""></label></div></td>
                                        <td>Muy lejos, más allá de las montañas de palabras, alejados de los países de las vocales y las consonantes, viven los textos simulados. Viven aislados en casas de letras, en la costa de la semántica, un gran océano de lenguas. Un riachuelo llamado Pons fluye por su pueblo y los abastece con las normas necesarias. Hablamos de un país paraisomático en el que a uno le caen pedazos de frases asadas en la boca. Ni siquiera los todopoderosos signos de puntuación dominan a los textos simulados; una vida, se puede decir, poco ortográfica. Pero un buen día, una pequeña línea de texto simulado, llamada Lorem Ipsum, decidió aventurarse y salir al vasto mundo de la gramática. El gran Oxmox le desanconsejó hacerlo, ya que esas tierras estaban llenas de comas malvadas, signos de interrogación salvajes y puntos y coma traicioneros, pero el texto simulado no se dejó atemorizar. Empacó sus siete versales, enfundó su inicial en el cinturón y se puso en camino. Cuando ya había escalado las primeras colinas de las montañas cursivas, se dio media vuelta para dirigir su mirada por última vez, hacia su ciudad natal Letralandia, el encabezamiento del pueblo Alfabeto y el subtítulo de su</td>
                                        <td>01/01/1970</td>
                                    </tr>
                                    <tr>
                                        <td><div class="checkbox"><label><input type="checkbox" value=""></label></div></td>
                                        <td>Muy lejos, más allá de las montañas de palabras, alejados de los países de las vocales y las consonantes, viven los textos simulados. Viven aislados en casas de letras, en la costa de la semántica, un gran océano de lenguas. Un riachuelo llamado Pons fluye por su pueblo y los abastece con las normas necesarias. Hablamos de un país paraisomático en el que a uno le caen pedazos de frases asadas en la boca. Ni siquiera los todopoderosos signos de puntuación dominan a los textos simulados; una vida, se puede decir, poco ortográfica. Pero un buen día, una pequeña línea de texto simulado, llamada Lorem Ipsum, decidió aventurarse y salir al vasto mundo de la gramática. El gran Oxmox le desanconsejó hacerlo, ya que esas tierras estaban llenas de comas malvadas, signos de interrogación salvajes y puntos y coma traicioneros, pero el texto simulado no se dejó atemorizar. Empacó sus siete versales, enfundó su inicial en el cinturón y se puso en camino. Cuando ya había escalado las primeras colinas de las montañas cursivas, se dio media vuelta para dirigir su mirada por última vez, hacia su ciudad natal Letralandia, el encabezamiento del pueblo Alfabeto y el subtítulo de su</td>
                                        <td>01/01/1970</td>
                                    </tr>
                                    <tr>
                                        <td><div class="checkbox"><label><input type="checkbox" value=""></label></div></td>
                                        <td>Muy lejos, más allá de las montañas de palabras, alejados de los países de las vocales y las consonantes, viven los textos simulados. Viven aislados en casas de letras, en la costa de la semántica, un gran océano de lenguas. Un riachuelo llamado Pons fluye por su pueblo y los abastece con las normas necesarias. Hablamos de un país paraisomático en el que a uno le caen pedazos de frases asadas en la boca. Ni siquiera los todopoderosos signos de puntuación dominan a los textos simulados; una vida, se puede decir, poco ortográfica. Pero un buen día, una pequeña línea de texto simulado, llamada Lorem Ipsum, decidió aventurarse y salir al vasto mundo de la gramática. El gran Oxmox le desanconsejó hacerlo, ya que esas tierras estaban llenas de comas malvadas, signos de interrogación salvajes y puntos y coma traicioneros, pero el texto simulado no se dejó atemorizar. Empacó sus siete versales, enfundó su inicial en el cinturón y se puso en camino. Cuando ya había escalado las primeras colinas de las montañas cursivas, se dio media vuelta para dirigir su mirada por última vez, hacia su ciudad natal Letralandia, el encabezamiento del pueblo Alfabeto y el subtítulo de su</td>
                                        <td>01/01/1970</td>
                                    </tr>
                                    -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-info" id="btnBorrar" disabled="disabled"> Borrar </button>
            </form>
            <div id="mensaje"></div>
        </div>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-3.1.0.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/centralOpiniones.js" charset="ISO-8859-1"></script>
    </body>
</html>
